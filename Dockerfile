FROM openjdk:11

ENV TZ "Europe/Rome"

ADD fonts fonts

RUN mkdir /pdf
RUN chmod -R 777 /pdf

RUN adduser --system --group spring
USER spring:spring

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
COPY elencoDipendenti.csv elencoDipendenti.csv
COPY anac.png anac.png

ENTRYPOINT ["java","-Dspring.profiles.active=master","-jar","/app.jar"]