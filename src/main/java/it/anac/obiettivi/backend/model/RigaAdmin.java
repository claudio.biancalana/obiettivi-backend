package it.anac.obiettivi.backend.model;

public class RigaAdmin {
	private String nome;
	private String cognome;
	private String ufficio;
	private String content_data;
	private String id;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getUfficio() {
		return ufficio;
	}
	public void setUfficio(String ufficio) {
		this.ufficio = ufficio;
	}
	public String getContent_data() {
		return content_data;
	}
	public void setContent_data(String content_data) {
		this.content_data = content_data;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}		
}
