package it.anac.obiettivi.backend.model.report;

public class Ru_riga_tot {
	private String categoria;
	private String nominativo;
	private double fte;
	
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getNominativo() {
		return nominativo;
	}
	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}
	public double getFte() {
		return fte;
	}
	public void setFte(double fte) {
		this.fte = fte;
	}
}
