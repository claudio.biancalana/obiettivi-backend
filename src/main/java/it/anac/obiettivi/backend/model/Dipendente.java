package it.anac.obiettivi.backend.model;

public class Dipendente {
	private String nome;
	private String qualifica;
	
	public Dipendente(String nome, String qualifica)
	{
		this.nome=nome;
		this.qualifica=qualifica;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getQualifica() {
		return qualifica;
	}

	public void setQualifica(String qualifica) {
		this.qualifica = qualifica;
	}
	
}
