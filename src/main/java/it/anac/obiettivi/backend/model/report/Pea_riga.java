package it.anac.obiettivi.backend.model.report;

public class Pea_riga {

	private int numero;
	private String descrizione;
	private String responsabile;
	private String meseInizio;
	private String meseFine;
	private int pesoFase;
	private String risultatiAttesi;
	private String percentualeMonitoraggio;
	private String noteMonitoraggio;
	
	private Ru_riga[] ru_riga;

	public String getPercentualeMonitoraggio() {
		return percentualeMonitoraggio;
	}

	public void setPercentualeMonitoraggio(String percentualeMonitoraggio) {
		this.percentualeMonitoraggio = percentualeMonitoraggio;
	}

	public String getNoteMonitoraggio() {
		return noteMonitoraggio;
	}

	public void setNoteMonitoraggio(String noteMonitoraggio) {
		this.noteMonitoraggio = noteMonitoraggio;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getResponsabile() {
		return responsabile;
	}

	public void setResponsabile(String responsabile) {
		this.responsabile = responsabile;
	}

	public String getMeseInizio() {
		return meseInizio;
	}

	public void setMeseInizio(String meseInizio) {
		this.meseInizio = meseInizio;
	}

	public String getMeseFine() {
		return meseFine;
	}

	public void setMeseFine(String meseFine) {
		this.meseFine = meseFine;
	}

	public int getPesoFase() {
		return pesoFase;
	}

	public void setPesoFase(int pesoFase) {
		this.pesoFase = pesoFase;
	}

	public String getRisultatiAttesi() {
		return risultatiAttesi;
	}

	public void setRisultatiAttesi(String risultatiAttesi) {
		this.risultatiAttesi = risultatiAttesi;
	}

	public Ru_riga[] getRu_riga() {
		return ru_riga;
	}

	public void setRu_riga(Ru_riga[] ru_riga) {
		this.ru_riga = ru_riga;
	}
}
