package it.anac.obiettivi.backend.model;

public class Dipendenti {
	private Dipendente[] dipendenti;
	
	public Dipendenti(Dipendente[] dipendenti)
	{
		this.dipendenti = dipendenti;		
	}

	public Dipendente[] getDipendenti() {
		return dipendenti;
	}

	public void setDipendenti(Dipendente[] dipendenti) {
		this.dipendenti = dipendenti;
	}
	
}
