package it.anac.obiettivi.backend.model.report;

public class Scheda {
	
	private String nome;
	private String cognome;
	
	private String ufficio;
	
	private String obiettivo_raggiunto;
	private String note_raggiungibile;
	
	private String[] uffici;
	
	private String tipologiaObiettivo;
	
	private String ambito;
	
	private String indicatore_efficienza;
	private String indicatore_efficacia;
	private String indicatore_prodotto;
	
	private String areaStrategica;
	private String obiettivoStrategico;
	private String obiettivoOperativo;
	
	private String obiettivoMiglioramento;
	
	private String gradoDiSignificativita;
	private String rilevanza;
	private String complessita;
	
	private String descrizioneProdotto;
	private String descrizioneEfficacia;
	private String descrizioneEfficienza;
	
	private String calcoloProdotto;
	private String calcoloEfficacia;
	private String calcoloEfficienza;
	
	private String targetProdotto;
	private String targetEfficacia;
	private String targetEfficienza;
	
	private String significativitaProdotto;
	private String significativitaEfficacia;
	private String significativitaEfficienza;
	
	private String dataInizio;
	private String dataFine;
	
	private RendicontazioneDiMonitoraggio monitoraggio;
	
	private String[] responsabili;
	
	private Pea_riga[] pea;
	
	private Ru_riga_tot[] ru_tot;
	
	public RendicontazioneDiMonitoraggio getMonitoraggio() {
		return monitoraggio;
	}

	public void setMonitoraggio(RendicontazioneDiMonitoraggio monitoraggio) {
		this.monitoraggio = monitoraggio;
	}

	public String getObiettivo_raggiunto() {
		return obiettivo_raggiunto;
	}
	
	public String getNote_raggiungibile() {
		return note_raggiungibile;
	}

	public void setNote_raggiungibile(String note_raggiungibile) {
		this.note_raggiungibile = note_raggiungibile;
	}

	public void setObiettivo_raggiunto(String obiettivo_raggiunto) {
		this.obiettivo_raggiunto = obiettivo_raggiunto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getUfficio() {
		return ufficio;
	}

	public void setUfficio(String ufficio) {
		this.ufficio = ufficio;
	}

	public String[] getUffici() {
		return uffici;
	}

	public void setUffici(String[] uffici) {
		this.uffici = uffici;
	}

	public String getAreaStrategica() {
		return areaStrategica;
	}

	public void setAreaStrategica(String areaStrategica) {
		this.areaStrategica = areaStrategica;
	}

	public String getObiettivoStrategico() {
		return obiettivoStrategico;
	}

	public void setObiettivoStrategico(String obiettivoStrategico) {
		this.obiettivoStrategico = obiettivoStrategico;
	}

	public String getObiettivoOperativo() {
		return obiettivoOperativo;
	}

	public void setObiettivoOperativo(String obiettivoOperativo) {
		this.obiettivoOperativo = obiettivoOperativo;
	}
	
	public String getObiettivoMiglioramento() {
		return obiettivoMiglioramento;
	}

	public void setObiettivoMiglioramento(String obiettivoMiglioramento) {
		this.obiettivoMiglioramento = obiettivoMiglioramento;
	}

	public String getGradoDiSignificativita() {
		return gradoDiSignificativita;
	}

	public void setGradoDiSignificativita(String gradoDiSignificativita) {
		this.gradoDiSignificativita = gradoDiSignificativita;
	}

	public String getRilevanza() {
		
		if (rilevanza.equals("1"))
			return "BASSA";
		if (rilevanza.equals("2"))
			return "MEDIA";
		if (rilevanza.equals("3"))
			return "ALTA";
		
		return rilevanza;
	}

	public void setRilevanza(String rilevanza) {
		this.rilevanza = rilevanza;
	}

	public String getComplessita() {
		
		if (complessita.equals("1"))
			return "BASSA";
		if (complessita.equals("2"))
			return "MEDIA";
		if (complessita.equals("3"))
			return "ALTA";
		
		return complessita;
	}

	public void setComplessita(String complessita) {
		this.complessita = complessita;
	}

	public String getDescrizioneProdotto() {
		return descrizioneProdotto;
	}

	public void setDescrizioneProdotto(String descrizioneProdotto) {
		this.descrizioneProdotto = descrizioneProdotto;
	}

	public String getDescrizioneEfficacia() {
		return descrizioneEfficacia;
	}

	public void setDescrizioneEfficacia(String descrizioneEfficacia) {
		this.descrizioneEfficacia = descrizioneEfficacia;
	}

	public String getDescrizioneEfficienza() {
		return descrizioneEfficienza;
	}

	public void setDescrizioneEfficienza(String descrizioneEfficienza) {
		this.descrizioneEfficienza = descrizioneEfficienza;
	}

	public String getCalcoloProdotto() {
		return calcoloProdotto;
	}

	public void setCalcoloProdotto(String calcoloProdotto) {
		this.calcoloProdotto = calcoloProdotto;
	}

	public String getCalcoloEfficacia() {
		return calcoloEfficacia;
	}

	public void setCalcoloEfficacia(String calcoloEfficacia) {
		this.calcoloEfficacia = calcoloEfficacia;
	}

	public String getCalcoloEfficienza() {
		return calcoloEfficienza;
	}

	public void setCalcoloEfficienza(String calcoloEfficienza) {
		this.calcoloEfficienza = calcoloEfficienza;
	}

	public String getTargetProdotto() {
		return targetProdotto;
	}

	public void setTargetProdotto(String targetProdotto) {
		this.targetProdotto = targetProdotto;
	}

	public String getTargetEfficacia() {
		return targetEfficacia;
	}

	public void setTargetEfficacia(String targetEfficacia) {
		this.targetEfficacia = targetEfficacia;
	}

	public String getTargetEfficienza() {
		return targetEfficienza;
	}

	public void setTargetEfficienza(String targetEfficienza) {
		this.targetEfficienza = targetEfficienza;
	}

	public String getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(String dataInizio) {
		this.dataInizio = dataInizio;
	}

	public String getDataFine() {
		return dataFine;
	}

	public void setDataFine(String dataFine) {
		this.dataFine = dataFine;
	}

	public String[] getResponsabili() {
		return responsabili;
	}

	public void setResponsabili(String[] responsabili) {
		this.responsabili = responsabili;
	}

	public Pea_riga[] getPea() {
		return pea;
	}

	public void setPea(Pea_riga[] pea) {
		this.pea = pea;
	}

	public Ru_riga_tot[] getRu_tot() {
		return ru_tot;
	}

	public void setRu_tot(Ru_riga_tot[] ru_tot) {
		this.ru_tot = ru_tot;
	}

	public String getSignificativitaProdotto() {
		return significativitaProdotto;
	}

	public void setSignificativitaProdotto(String significativitaProdotto) {
		this.significativitaProdotto = significativitaProdotto;
	}

	public String getSignificativitaEfficacia() {
		return significativitaEfficacia;
	}

	public void setSignificativitaEfficacia(String significativitaEfficacia) {
		this.significativitaEfficacia = significativitaEfficacia;
	}

	public String getSignificativitaEfficienza() {
		return significativitaEfficienza;
	}

	public void setSignificativitaEfficienza(String significativitaEfficienza) {
		this.significativitaEfficienza = significativitaEfficienza;
	}

	public String getTipologiaObiettivo() {
		return tipologiaObiettivo;
	}

	public void setTipologiaObiettivo(String tipologiaObiettivo) {
		this.tipologiaObiettivo = tipologiaObiettivo;
	}

	public String getIndicatore_efficienza() {
		return indicatore_efficienza;
	}

	public void setIndicatore_efficienza(String indicatore_efficienza) {
		this.indicatore_efficienza = indicatore_efficienza;
	}

	public String getIndicatore_efficacia() {
		return indicatore_efficacia;
	}

	public void setIndicatore_efficacia(String indicatore_efficacia) {
		this.indicatore_efficacia = indicatore_efficacia;
	}

	public String getIndicatore_prodotto() {
		return indicatore_prodotto;
	}

	public void setIndicatore_prodotto(String indicatore_prodotto) {
		this.indicatore_prodotto = indicatore_prodotto;
	}

	public String getAmbito() {
		return ambito;
	}

	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}
}
