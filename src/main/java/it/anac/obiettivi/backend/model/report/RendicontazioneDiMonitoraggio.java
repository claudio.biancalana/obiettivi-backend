package it.anac.obiettivi.backend.model.report;

public class RendicontazioneDiMonitoraggio {
	private String efficaciaLivelloDiRealizzazione;
	private String efficaciaNoteLivelloDiRealizzazione;
	private int efficienzaMesiUtilizzati;
	private int efficienzaMesiUtili;
	private String prodottoLivelloDiRealizzazione;
	private String prodottoNoteLivelloDiRealizzazione;
	private String efficienzaLivelloDiRealizzazione;
	private String efficienzaNoteLivelloDiRealizzazione;
	private String efficaciaNumeratore;
	private String efficaciaDenominatore;
	public String getEfficaciaLivelloDiRealizzazione() {
		return efficaciaLivelloDiRealizzazione;
	}
	public void setEfficaciaLivelloDiRealizzazione(String efficaciaLivelloDiRealizzazione) {
		this.efficaciaLivelloDiRealizzazione = efficaciaLivelloDiRealizzazione;
	}
	public String getEfficaciaNoteLivelloDiRealizzazione() {
		return efficaciaNoteLivelloDiRealizzazione;
	}
	public void setEfficaciaNoteLivelloDiRealizzazione(String efficaciaNoteLivelloDiRealizzazione) {
		this.efficaciaNoteLivelloDiRealizzazione = efficaciaNoteLivelloDiRealizzazione;
	}
	public int getEfficienzaMesiUtilizzati() {
		return efficienzaMesiUtilizzati;
	}
	public void setEfficienzaMesiUtilizzati(int efficienzaMesiUtilizzati) {
		this.efficienzaMesiUtilizzati = efficienzaMesiUtilizzati;
	}
	public int getEfficienzaMesiUtili() {
		return efficienzaMesiUtili;
	}
	public void setEfficienzaMesiUtili(int efficienzaMesiUtili) {
		this.efficienzaMesiUtili = efficienzaMesiUtili;
	}
	public String getProdottoLivelloDiRealizzazione() {
		return prodottoLivelloDiRealizzazione;
	}
	public void setProdottoLivelloDiRealizzazione(String prodottoLivelloDiRealizzazione) {
		this.prodottoLivelloDiRealizzazione = prodottoLivelloDiRealizzazione;
	}
	public String getProdottoNoteLivelloDiRealizzazione() {
		return prodottoNoteLivelloDiRealizzazione;
	}
	public void setProdottoNoteLivelloDiRealizzazione(String prodottoNoteLivelloDiRealizzazione) {
		this.prodottoNoteLivelloDiRealizzazione = prodottoNoteLivelloDiRealizzazione;
	}
	public String getEfficienzaLivelloDiRealizzazione() {
		return efficienzaLivelloDiRealizzazione;
	}
	public void setEfficienzaLivelloDiRealizzazione(String efficienzaLivelloDiRealizzazione) {
		this.efficienzaLivelloDiRealizzazione = efficienzaLivelloDiRealizzazione;
	}
	public String getEfficienzaNoteLivelloDiRealizzazione() {
		return efficienzaNoteLivelloDiRealizzazione;
	}
	public void setEfficienzaNoteLivelloDiRealizzazione(String efficienzaNoteLivelloDiRealizzazione) {
		this.efficienzaNoteLivelloDiRealizzazione = efficienzaNoteLivelloDiRealizzazione;
	}
	public String getEfficaciaNumeratore() {
		return efficaciaNumeratore;
	}
	public void setEfficaciaNumeratore(String efficaciaNumeratore) {
		this.efficaciaNumeratore = efficaciaNumeratore;
	}
	public String getEfficaciaDenominatore() {
		return efficaciaDenominatore;
	}
	public void setEfficaciaDenominatore(String efficaciaDenominatore) {
		this.efficaciaDenominatore = efficaciaDenominatore;
	}
	
	
	
}
