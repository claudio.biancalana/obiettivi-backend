package it.anac.obiettivi.backend.tool;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.opencsv.exceptions.CsvValidationException;

import it.anac.obiettivi.backend.engine.ReportHelper;
import it.anac.obiettivi.backend.engine.Transcoding;
import it.anac.obiettivi.backend.model.report.Pea_riga;
import it.anac.obiettivi.backend.model.report.Ru_riga;
import it.anac.obiettivi.backend.model.report.Ru_riga_tot;
import it.anac.obiettivi.backend.model.report.Scheda;

public class ExcelUtil
{	
	private static XSSFWorkbook workbook = new XSSFWorkbook();
	
    private static XSSFSheet sottomissioni = workbook.createSheet("Matrice I");
    private static XSSFSheet fasi = workbook.createSheet("Matrice II");
    private static XSSFSheet risorse_umane = workbook.createSheet("Matrice III");
    
    private static int c = 0;
    private static int offset = 0;
    private static int contatore = 1;
    
    private static String filename;

	public static void start(String aux) throws IOException, ParseException, CsvValidationException
	{
		filename = aux;
		byte[] encoded = Files.readAllBytes(Paths.get("db_file/submissions_board.json"));
		String json = new String(encoded);
		
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(json);
	    JSONArray tot_jo = (JSONArray) obj;
	    
	    for(int i=0; i<tot_jo.size(); i++)
	    {
	    	JSONObject aux_jo = (JSONObject)tot_jo.get(i);   	
	    	String aux_json = aux_jo.toJSONString();  	
	    	ReportHelper rh = new ReportHelper();
	    	
	    	int obb = 0;

	    	Scheda scheda_1 = null;
	    	Scheda scheda_2 = null;
	    	try
	    	{
	    		obb = rh.getObiettivi(aux_json);
	    		if (obb==1)
	    		{
	    			scheda_1 = rh.getSchedaFromJson_ob_1(aux_json, 10);
	    			scheda_2 = null;
	    		}
	    		else
	    		{
	    			scheda_1 = rh.getSchedaFromJson_ob_1(aux_json, 10);
	    			scheda_2 = rh.getSchedaFromJson_ob_2(aux_json, 10);
	    		}
	
	    	} catch (Exception ex)
	    	{
	    		// non compilato quindi salto
	    	}
	    	
	    	if (scheda_1!=null)
	    	{
	    		schedaToExcel(scheda_1);	    		
	    	}   	
	    	if (scheda_2!=null)
	    	{
	    		schedaToExcel(scheda_2);
	    	}
	    }
	    end();
	}
	
	
	private static String flatArray(String[] aux)
	{
		String ret = aux[0];
		
		for(int i=1; i<aux.length; i++)
			ret = ret+System.getProperty("line.separator")+aux[i];
		
		return ret;
	}
	
	private static String flatArrayUfficio(String[] aux) throws CsvValidationException, IOException
	{
		Transcoding t = new Transcoding();
		
		String ret = t.getUfficioFromAcronimo(aux[0]);
		
		for(int i=1; i<aux.length; i++)
			ret = ret+ System.getProperty("line.separator")+t.getUfficioFromAcronimo(aux[i]);
		
		return ret;
	}
	
	public static void setHeader()
	{
		 Row row = sottomissioni.createRow(0);
		 row.createCell(0).setCellValue("ID OBIETTIVO");
		 row.createCell(1).setCellValue("NOME");
		 row.createCell(2).setCellValue("COGNOME");
		 row.createCell(3).setCellValue("UFFICIO");
		 row.createCell(4).setCellValue("ID AREA STRATEGICA");
		 row.createCell(5).setCellValue("AREA STRATEGICA");
		 row.createCell(6).setCellValue("ID OBIETTIVO STRATEGICO");
		 row.createCell(7).setCellValue("OBIETTIVO STRATEGICO");
		 row.createCell(8).setCellValue("ACRONIMO UFFICI/O");
		 row.createCell(9).setCellValue("NOME UFFICI/O");
		 row.createCell(10).setCellValue("RESPONSABILE/I");
		 row.createCell(11).setCellValue("OBIETTIVO OPERATIVO");
		 row.createCell(12).setCellValue("OBIETTIVO MIGLIORAMENTO");
		 row.createCell(13).setCellValue("RILEVANZA");
		 row.createCell(14).setCellValue("COMPLESSITA'");
		 row.createCell(15).setCellValue("SIGNIFICATIVITA'");
		 row.createCell(16).setCellValue("DATA INIZIO");
		 row.createCell(17).setCellValue("DATA FINE");
		 row.createCell(18).setCellValue("DESCRIZIONE PRODOTTO");
		 row.createCell(19).setCellValue("CALCOLO PRODOTTO");
		 row.createCell(20).setCellValue("TARGET PRODOTTO");
		 row.createCell(21).setCellValue("DESCRIZIONE EFFICACIA");
		 row.createCell(22).setCellValue("CALCOLO EFFICACIA");
		 row.createCell(23).setCellValue("TARGET EFFICACIA");
		 row.createCell(24).setCellValue("DESCRIZIONE EFFICIENZA");
		 row.createCell(25).setCellValue("CALCOLO EFFICIENZA");
		 row.createCell(26).setCellValue("TARGET EFFICIENZA");

		 Row fase = fasi.createRow(0);
		 fase.createCell(0).setCellValue("ID OBIETTIVO");
		 fase.createCell(1).setCellValue("UFFICIO/I");
		 fase.createCell(2).setCellValue("UFFICIO/I (SIGLA)");
		 fase.createCell(3).setCellValue("DIRIGENTE RESPONSABILE");
		 fase.createCell(4).setCellValue("DESCRIZIONE OBIETTIVO");
		 fase.createCell(5).setCellValue("N.FASE");
		 fase.createCell(6).setCellValue("DESCRIZIONE FASE");
		 fase.createCell(7).setCellValue("RESPONSABILE FASE");
		 fase.createCell(8).setCellValue("MESE");
		 //fase.createCell(9).setCellValue("MESE FINALE DI RIFERIMENTO");
		 fase.createCell(9).setCellValue("PESO FASE");
		 fase.createCell(10).setCellValue("RISULTATI ATTESI FASE");
		 fase.createCell(11).setCellValue("CATEGORIA");
		 fase.createCell(12).setCellValue("NOMINATIVO");
		 fase.createCell(13).setCellValue("PARTECIPAZIONE ALLA FASE (%)");
		 fase.createCell(14).setCellValue("FTE DI FASE (%)");
		 
		 Row ru = risorse_umane.createRow(0);
		 ru.createCell(0).setCellValue("ID OBIETTIVO");
		 ru.createCell(1).setCellValue("UFFICIO/I");
		 ru.createCell(2).setCellValue("UFFICIO/I (SIGLA)");
		 ru.createCell(3).setCellValue("DIRIGENTE RESPONSABILE/I");
		 ru.createCell(4).setCellValue("DESCRIZIONE OBIETTIVO");
		 ru.createCell(5).setCellValue("CATEGORIA");
		 ru.createCell(6).setCellValue("NOMINATIVO");
		 ru.createCell(7).setCellValue("FTE (%)");
	}
	
	public static String getAreaStrategicaCode(String val)
	{
		if (val.toLowerCase().contains("anticorruzione"))
			return "1";
		if (val.toLowerCase().contains("contratti"))
			return "2";
		if (val.toLowerCase().contains("gestione"))
			return "3";
		return "-";
	}
	
	public static String getObiettivoStrategicoCode(String val)
	{
		if (val.toLowerCase().contains("1.1"))
			return "1.1";
		if (val.toLowerCase().contains("2.1"))
			return "2.1";
		if (val.toLowerCase().contains("2.2"))
			return "2.2";
		if (val.toLowerCase().contains("3.1"))
			return "3.1";
		if (val.toLowerCase().contains("3.2"))
			return "3.2";
		return "-";
	}
	
	public static void schedaToExcel(Scheda scheda) throws CsvValidationException, IOException
	{
		if (c==0)
			setHeader();
		
		c++;
		
	     Row row = sottomissioni.createRow(c);
	     row.createCell(0).setCellValue(c);
	     row.createCell(1).setCellValue(scheda.getNome());
	     row.createCell(2).setCellValue(scheda.getCognome());
	     row.createCell(3).setCellValue(scheda.getUfficio());
	     row.createCell(4).setCellValue(getAreaStrategicaCode(scheda.getAreaStrategica()));
	     row.createCell(5).setCellValue(scheda.getAreaStrategica());
	     row.createCell(6).setCellValue(getObiettivoStrategicoCode(scheda.getObiettivoStrategico()));
	     row.createCell(7).setCellValue(scheda.getObiettivoStrategico());
	     	     
	     Cell cellUffici = row.createCell(8);
	     cellUffici.setCellValue(flatArray(scheda.getUffici()));
	     CellStyle cs = workbook.createCellStyle();
	     cs.setWrapText(true);
	     cellUffici.setCellStyle(cs);
	     
	     Cell cellUfficiDesc = row.createCell(9);
	     cellUfficiDesc.setCellValue(flatArrayUfficio(scheda.getUffici()));
	     cs = workbook.createCellStyle();
	     cs.setWrapText(true);
	     cellUfficiDesc.setCellStyle(cs);
	     
	     Cell cellResponsabili = row.createCell(10);
	     cellResponsabili.setCellValue(flatArray(scheda.getResponsabili()));
	     cs = workbook.createCellStyle();
	     cs.setWrapText(true);
	     cellResponsabili.setCellStyle(cs);
	     
	     row.createCell(11).setCellValue(scheda.getObiettivoOperativo());
	     row.createCell(12).setCellValue(scheda.getObiettivoMiglioramento());
	     row.createCell(13).setCellValue(scheda.getRilevanza());
	     row.createCell(14).setCellValue(scheda.getComplessita());
	     row.createCell(15).setCellValue(scheda.getGradoDiSignificativita());
	     row.createCell(16).setCellValue(scheda.getDataInizio());
	     row.createCell(17).setCellValue(scheda.getDataFine());
	     row.createCell(18).setCellValue(scheda.getDescrizioneProdotto());
	     row.createCell(19).setCellValue(scheda.getCalcoloProdotto());
	     row.createCell(20).setCellValue(scheda.getTargetProdotto());
	     row.createCell(21).setCellValue(scheda.getDescrizioneEfficacia());
	     row.createCell(22).setCellValue(scheda.getCalcoloEfficacia());
	     row.createCell(23).setCellValue(scheda.getTargetEfficienza());
	     row.createCell(24).setCellValue(scheda.getDescrizioneEfficienza());
	     row.createCell(25).setCellValue(scheda.getCalcoloEfficienza());
	     row.createCell(26).setCellValue(scheda.getTargetEfficacia()); 
	     
	     for (int i=0; i<scheda.getPea().length; i++)
	     { 
	    	 Pea_riga pr = scheda.getPea()[i];
	    	 
	    	 for (int j=0; j<pr.getRu_riga().length; j++)
	    	 {
	    		 int start_mese = getMese(pr.getMeseInizio());
	    		 int end_mese = getMese(pr.getMeseFine());
	    		 
	    		 int diff_mese = end_mese-start_mese+1;
	    		 
	    		 for (int k=0; k<diff_mese; k++)
	    		 {	 
				     Row row_fase = fasi.createRow(1+offset+j+k);
				     		    	 
			    	 row_fase.createCell(0).setCellValue(c);
			    	 
			    	 Cell cellUfficiDescr = row_fase.createCell(1);
				     cellUfficiDescr.setCellValue(flatArrayUfficio(scheda.getUffici()));
				     cs = workbook.createCellStyle();
				     cs.setWrapText(true);
				     cellUfficiDescr.setCellStyle(cs);
				     
				     Cell cellUfficiAcron = row_fase.createCell(2);
				     cellUfficiAcron.setCellValue(flatArray(scheda.getUffici()));
				     cs = workbook.createCellStyle();
				     cs.setWrapText(true);
				     cellUfficiAcron.setCellStyle(cs);
			    	 
			    	 row_fase.createCell(3).setCellValue(pr.getResponsabile());
			    	 
			    	 String ob = "";
			    	 if (scheda.getObiettivoOperativo()!=null)
			    		  ob = scheda.getObiettivoOperativo();
			    	 else ob = scheda.getObiettivoMiglioramento();
			    	 row_fase.createCell(4).setCellValue(ob);
			    	 
			    	 row_fase.createCell(5).setCellValue(i+1);
			    	 row_fase.createCell(6).setCellValue(pr.getDescrizione());
			    	 row_fase.createCell(7).setCellValue(pr.getResponsabile());
			    	 row_fase.createCell(8).setCellValue(getMese(start_mese));
			    	 start_mese = start_mese +1;
			    	 //row_fase.createCell(9).setCellValue(pr.getMeseFine());
			    	 row_fase.createCell(9).setCellValue(pr.getPesoFase());
			    	 row_fase.createCell(10).setCellValue(pr.getRisultatiAttesi());
		    		 
		    		 Ru_riga ru_riga = pr.getRu_riga()[j];
		    		     		 
		    		 row_fase.createCell(11).setCellValue(ru_riga.getCategoria());
		    		 row_fase.createCell(12).setCellValue(ru_riga.getNominativo());
		    		 row_fase.createCell(13).setCellValue(ru_riga.getPartecipazione());
		    		 row_fase.createCell(14).setCellValue(ru_riga.getFte());
	    		 }
	    		 offset = offset +diff_mese-1;
	    	 }	    
	    	 offset=offset+pr.getRu_riga().length;
	     }

	     
	     for (int i=0; i<scheda.getRu_tot().length; i++)
	     { 
 		     Row row_ru = risorse_umane.createRow(contatore);
		    	 
	    	 row_ru.createCell(0).setCellValue(c);
	    	
	    	 Cell cellUfficiDescr = row_ru.createCell(1);
		     cellUfficiDescr.setCellValue(flatArrayUfficio(scheda.getUffici()));
		     cs = workbook.createCellStyle();
		     cs.setWrapText(true);
		     cellUfficiDescr.setCellStyle(cs);
		     
		     Cell cellUfficiAcron = row_ru.createCell(2);
		     cellUfficiAcron.setCellValue(flatArray(scheda.getUffici()));
		     cs = workbook.createCellStyle();
		     cs.setWrapText(true);
		     cellUfficiAcron.setCellStyle(cs);
	    	 
		     // TO DO: tutti i responsabili
	    	 row_ru.createCell(3).setCellValue(flatArray(scheda.getResponsabili()));
	    	 
	    	 String ob = "";
	    	 if (scheda.getObiettivoOperativo()!=null)
	    		  ob = scheda.getObiettivoOperativo();
	    	 else ob = scheda.getObiettivoMiglioramento();
	    	 row_ru.createCell(4).setCellValue(ob);
	    	 
	    	 
	    	 Ru_riga_tot ru_row = scheda.getRu_tot()[i];
	    	 
	    	 row_ru.createCell(5).setCellValue(ru_row.getCategoria());
		 	 row_ru.createCell(6).setCellValue(ru_row.getNominativo());
		     row_ru.createCell(7).setCellValue(ru_row.getFte());
		     
		     contatore++;

	     }  
	    
	}
	
	private static String getMese(int mese)
	{
		String ret = "";
		if (mese==1)
			ret = "Gennaio 2022";
		if (mese==2)
			ret = "Febbraio 2022";
		if (mese==3)
			ret = "Marzo 2022";
		if (mese==4)
			ret = "Aprile 2022";
		if (mese==5)
			ret = "Maggio 2022";
		if (mese==6)
			ret = "Giugno 2022";
		if (mese==7)
			ret = "Luglio 2022";
		if (mese==8)
			ret = "Agosto 2022";
		if (mese==9)
			ret = "Settembre 2022";
		if (mese==10)
			ret = "Ottobre 2022";
		if (mese==11)
			ret = "Novembre 2022";
		if (mese==12)
			ret = "Dicembre 2022";
		
		return ret;
	}
	
	private static int getMese(String mese)
	{
		int ret = -1;
		if (mese.toLowerCase().contains("gennaio"))
			ret = 1;
		if (mese.toLowerCase().contains("febbraio"))
			ret = 2;
		if (mese.toLowerCase().contains("marzo"))
			ret = 3;
		if (mese.toLowerCase().contains("aprile"))
			ret = 4;
		if (mese.toLowerCase().contains("maggio"))
			ret = 5;
		if (mese.toLowerCase().contains("giugno"))
			ret = 6;
		if (mese.toLowerCase().contains("luglio"))
			ret = 7;
		if (mese.toLowerCase().contains("agosto"))
			ret = 8;
		if (mese.toLowerCase().contains("settembre"))
			ret = 9;
		if (mese.toLowerCase().contains("ottobre"))
			ret = 10;
		if (mese.toLowerCase().contains("novembre"))
			ret = 11;
		if (mese.toLowerCase().contains("dicembre"))
			ret = 12;
		
		return ret;
	}
	
	public static void main(String[] argv) throws CsvValidationException, IOException, ParseException
	{
		System.out.println("START TEST");
		
		ExcelUtil.start("test_2.xlsx");
		
		System.out.println("END TEST");
	}
	

	public static void end()
	
	{
	     try {
	         FileOutputStream out = new FileOutputStream(
	             new File(filename));
	         	         
	         workbook.write(out);
	         out.close();
	     } catch (Exception e) {
	         e.printStackTrace();
	     }
	}
}