package it.anac.obiettivi.backend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ObiettiviBackendApplication {
	private static final Logger LOGGER = LoggerFactory.getLogger(ObiettiviBackendApplication.class);
	
	
	
	@Autowired
	private Environment environment;
	    
	private String getActiveProfiles()
	{
		String out = "";
		
	    for (String profileName : environment.getActiveProfiles())
	            out = out +" " + profileName; 
	        
	    return out.trim();
	}
	
	@Value("${obiettivi.versione}")
    private String versione;
	
	@Value("${obiettivi.ambiente}")
    private String ambiente;
	
	@Value("${obiettivi.release}")
    private String release;

	public static void main(String[] args) {
		SpringApplication.run(ObiettiviBackendApplication.class, args);
	}
	
	@GetMapping("/")
	public String landingPage()
	{
		String startInfo = " OBIETTIVI 2022 - BACKEND API <a href=\"swagger-ui/index.html\">SWAGGER</a><br>Versione: "+versione+"@"+getActiveProfiles();
		startInfo = startInfo+"\n"+" Ambiente: "+ambiente+"_"+release;
		LOGGER.info("Segnalazioni: "+versione+"@"+getActiveProfiles()+" Ambiente: "+ambiente+"_"+release);	
		
		
	    return startInfo;
	}

}
