package it.anac.obiettivi.backend.engine;

import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.client.result.UpdateResult;

import it.anac.obiettivi.backend.model.Obiettivi;
import it.anac.obiettivi.backend.model.RigaAdmin;
import it.anac.obiettivi.backend.rest.RestConsts;

@RestController
@RequestMapping(value = { "/ws" })
public class MongoHelper extends RestConsts {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoHelper.class);
		
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Value("${obiettivi.idform}")
    private String idForm_obiettivi;
	
	@Value("${obiettivi.prd.idform}")
    private String idForm_prd_obiettivi;
	
	@Value("${obiettivi.ambiente}")
    private String AMBIENTE;
		
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@RequestMapping(value = "/update", method = RequestMethod.GET)	
	public void update(String id, String key, String val)  {
				
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(id));
		mongoTemplate.updateFirst(
				query, 
				Update.update(key, val),
				"submissions");

	}
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@RequestMapping(value = "/reopen", method = RequestMethod.GET)	
	public RedirectView reopen(String id, String key, String val)  {
				
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(id));
		mongoTemplate.updateFirst(
				query, 
				Update.update(key, val),
				"submissions");
		
		Query query2 = new Query();
		query2.addCriteria(Criteria.where("_id").is(id));
		mongoTemplate.updateFirst(
				query2, 
				Update.update("data.content_data", ""), // scompare il content_data a fronte della riapertura della scheda.
				"submissions");
		
		return new RedirectView("../../?login="+id);
	}
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@RequestMapping(value = "/admin", method = RequestMethod.GET)	
	public List<RigaAdmin> getRighe() throws JsonProcessingException, ParseException {
		
		Query query = new Query();
		//query.addCriteria(Criteria.where("form").is(idForm_obiettivi));
		List<String> jsons = mongoTemplate.find(query,String.class, "submissions");
		
		List<RigaAdmin> res = new LinkedList<RigaAdmin>();
		
		for(int i=0; i<jsons.size(); i++)
		{
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(jsons.get(i));
			JSONObject tot_jo = (JSONObject) obj;
			        
			JSONObject jo = (JSONObject) tot_jo.get("data");
			
			String idFormAux = idForm_obiettivi;
			if (AMBIENTE.equalsIgnoreCase("produzione"))
				idFormAux = idForm_prd_obiettivi;
		    
			System.out.println("FORM: "+((String)((JSONObject)tot_jo.get("form")).get("$oid")));
			if (((String)((JSONObject)tot_jo.get("form")).get("$oid")).trim().equals(idFormAux) &&
			   (((Double)tot_jo.get("deleted")))==null)
			{
				RigaAdmin adm_row = new RigaAdmin();
				adm_row.setNome((String)jo.get("nome"));
			    adm_row.setCognome((String)jo.get("cognome"));
				adm_row.setContent_data((String)jo.get("content_data"));
				adm_row.setUfficio((String)jo.get("ufficio"));
				adm_row.setId(((String)((JSONObject)tot_jo.get("_id")).get("$oid")));
				res.add(adm_row);	
			}		
		}
		return res;
	}
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@RequestMapping(value = "/obiettivi", method = RequestMethod.GET)	
	public Obiettivi getOjb(String id) throws ParseException  {
				
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(id));
		String json = mongoTemplate.findOne(query,String.class, "submissions");		
		
		JSONParser parser = new JSONParser();
		
		Object obj = parser.parse(json);
        JSONObject tot_jo = (JSONObject) obj;
    
        JSONObject jo = (JSONObject) tot_jo.get("data");
        
        Obiettivi obiettivi = new Obiettivi();
        obiettivi.setNumero(""+(long)jo.get("obiettivi_da_compilare"));
        return obiettivi;
	}
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@RequestMapping(value = "/salvainbozza/{id}", method = RequestMethod.POST)
	public @ResponseBody UpdateResult save(@RequestBody String json,
			                               @PathVariable("id") String id) throws ParseException  {
					
		LOGGER.info("ID: "+id);
		LOGGER.info("json: "+json);
		
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(id));
		Update update = new Update();
		
		JSONParser parser = new JSONParser();
		
		Object obj = parser.parse(json);
        JSONObject tot_jo = (JSONObject) obj;
        
		
		return mongoTemplate.updateFirst(query,update.set("data", tot_jo),JSONObject.class, "submissions");		
	}
}