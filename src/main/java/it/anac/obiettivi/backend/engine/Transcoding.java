package it.anac.obiettivi.backend.engine;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import org.apache.commons.text.StringTokenizer;
import org.springframework.stereotype.Service;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;

import it.anac.obiettivi.backend.model.Dipendente;
import it.anac.obiettivi.backend.model.Dipendenti;

@Service
public class Transcoding {
	
	private final String ORGANIZZAZIONE = "elencoDipendenti.csv";
	
	public Dipendenti getDirigenteFromUfficio(String acronimoUfficio) throws CsvValidationException, IOException
	{
		FileReader reader;
		CSVParser parser;
		CSVReader csvReader;
		
	    LinkedList<Dipendente> res = new LinkedList<Dipendente>();
		
	    StringTokenizer st = new StringTokenizer(acronimoUfficio,",");
	    while(st.hasNext())
	    {
	    	String aux = st.nextToken().trim();

			reader = new FileReader(new File(ORGANIZZAZIONE));
			
			parser = new CSVParserBuilder()
				    .withSeparator(';')
				    .withIgnoreQuotations(true)
				    .build();
	
		    csvReader = new CSVReaderBuilder(reader)
				    .withSkipLines(0)
				    .withCSVParser(parser)
				    .build();

		    String[] line;
		    while ((line = csvReader.readNext()) != null)
		    	if (line[7].toLowerCase().equals(aux.toLowerCase()) &&
				    line[4].toLowerCase().equals("dirigente".toLowerCase()))
		    			res.add(new Dipendente(line[0],line[4]));
		    
			csvReader.close();
			reader.close();
		    
	    }
	    
	    Dipendente[] dipendenti = new Dipendente[res.size()];
	    for (int i=0; i<res.size(); i++)
	    	dipendenti[i]=res.get(i);
	    	 
	    

		    
	    
	    return new Dipendenti(dipendenti);
	}
	
	public Dipendenti getDipendentiFromUfficio(String acronimoUfficio) throws CsvValidationException, IOException
	{		
		FileReader reader;
		CSVParser parser;
		CSVReader csvReader;
		
	    LinkedList<Dipendente> res = new LinkedList<Dipendente>();
	    
	    System.out.println("getDipendentiFromUfficio -> ACRONIMI: "+acronimoUfficio);
		
	    StringTokenizer st = new StringTokenizer(acronimoUfficio,",");
	    while(st.hasNext())
	    {
	    	String aux = st.nextToken().trim();
	    	
			reader = new FileReader(new File(ORGANIZZAZIONE));
			
			parser = new CSVParserBuilder()
				    .withSeparator(';')
				    .withIgnoreQuotations(true)
				    .build();
	
		    csvReader = new CSVReaderBuilder(reader)
				    .withSkipLines(0)
				    .withCSVParser(parser)
				    .build();
	    	    
		    String[] line;

		    while ((line = csvReader.readNext()) != null)
		    {
		    	if (line[7].toLowerCase().equals(aux.toLowerCase()))
		    			res.add(new Dipendente(line[0],line[4]));
		    }
		    
			csvReader.close();
			reader.close();
	    }
	    
	    Dipendente[] dipendenti = new Dipendente[res.size()];
	    for (int i=0; i<res.size(); i++)
	    	dipendenti[i]=res.get(i);
	    	
	    
	    
	    return new Dipendenti(dipendenti);   
	}

	public Dipendenti getFunzionariFromUfficio(String ufficio) throws CsvValidationException, IOException
	{		
		FileReader reader;
		CSVParser parser;
		CSVReader csvReader;
		
	    LinkedList<Dipendente> res = new LinkedList<Dipendente>();
	    
	    StringTokenizer st = new StringTokenizer(ufficio,",");
	    while(st.hasNext())
	    {
	    	String aux = st.nextToken().trim();
			reader = new FileReader(new File(ORGANIZZAZIONE));
			
			parser = new CSVParserBuilder()
				    .withSeparator(';')
				    .withIgnoreQuotations(true)
				    .build();
	
		    csvReader = new CSVReaderBuilder(reader)
				    .withSkipLines(0)
				    .withCSVParser(parser)
				    .build();
		    		    		
		    String[] line;
		    while ((line = csvReader.readNext()) != null)
		    	if (line[7].toLowerCase().equals(aux.toLowerCase()) &&
				    	line[4].toLowerCase().equals("funzionario".toLowerCase()))
		    			res.add(new Dipendente(line[0],line[4]));
		    
			   csvReader.close();
			   reader.close();
			    
	    }
	    
	    Dipendente[] dipendenti = new Dipendente[res.size()];
	    for (int i=0; i<res.size(); i++)
	    	dipendenti[i]=res.get(i);
	    	
	    
	    return new Dipendenti(dipendenti);   
	}
	
	public String getUfficioFromCode(String acronimo) throws CsvValidationException, IOException
	{
		FileReader reader;
		CSVParser parser;
		CSVReader csvReader;
		
		StringTokenizer st = new StringTokenizer(acronimo,",");
	    while(st.hasNext())
	    {
	    	String aux = st.nextToken().trim();
			reader = new FileReader(new File(ORGANIZZAZIONE));
			
			parser = new CSVParserBuilder()
				    .withSeparator(';')
				    .withIgnoreQuotations(true)
				    .build();
	
		    csvReader = new CSVReaderBuilder(reader)
				    .withSkipLines(0)
				    .withCSVParser(parser)
				    .build();
		    		    		
		    String[] line;
		    while ((line = csvReader.readNext()) != null)
		    	if (line[7].toLowerCase().equals(aux.toLowerCase()))
		    			return acronimo+" - "+line[6];
		    
			   csvReader.close();
			   reader.close();
	    }
	    

		    
	    
	    return acronimo;				
	}

	public String getMailFromDirigente(String dirigente) throws CsvValidationException, IOException
	{
		FileReader reader;
		CSVParser parser;
		CSVReader csvReader;
		
		reader = new FileReader(new File(ORGANIZZAZIONE));
		
		parser = new CSVParserBuilder()
			    .withSeparator(';')
			    .withIgnoreQuotations(true)
			    .build();

	    csvReader = new CSVReaderBuilder(reader)
			    .withSkipLines(0)
			    .withCSVParser(parser)
			    .build();

	    String[] line;
	    while ((line = csvReader.readNext()) != null)
	    {
	    	if (line[0].toLowerCase().equals(dirigente.toLowerCase()) &&
			    	(line[4].toLowerCase().equals("dirigente".toLowerCase())))
	    		return line[3];
	    }
	    
		csvReader.close();
		reader.close();
		    
		
	    return "";
	}
	
	public Dipendenti getImpiegatiFromUfficio(String ufficio) throws CsvValidationException, IOException
	{
		FileReader reader;
		CSVParser parser;
		CSVReader csvReader;
		
	    LinkedList<Dipendente> res = new LinkedList<Dipendente>();
	    
	    StringTokenizer st = new StringTokenizer(ufficio,",");
	    while(st.hasNext())
	    {
	    	String aux = st.nextToken().trim();
			reader = new FileReader(new File(ORGANIZZAZIONE));
			
			parser = new CSVParserBuilder()
				    .withSeparator(';')
				    .withIgnoreQuotations(true)
				    .build();
	
		    csvReader = new CSVReaderBuilder(reader)
				    .withSkipLines(0)
				    .withCSVParser(parser)
				    .build();
		    
	
		    String[] line;
		    while ((line = csvReader.readNext()) != null)
		    	if (line[7].toLowerCase().equals(aux.toLowerCase()) &&
				    	(line[4].toLowerCase().equals("impiegato".toLowerCase())))
		    			res.add(new Dipendente(line[0],line[4]));
		    
			   csvReader.close();
			   reader.close();
	    }
	    
	    Dipendente[] dipendenti = new Dipendente[res.size()];
	    for (int i=0; i<res.size(); i++)
	    	dipendenti[i]=res.get(i);

	    return new Dipendenti(dipendenti);   
	}
	
	public String getUfficioFromAcronimo(String acronimo) throws CsvValidationException, IOException
	{
		String ret  ="";
		FileReader reader;
		CSVParser parser;
		CSVReader csvReader;
			    
	    	String aux = acronimo;
			reader = new FileReader(new File(ORGANIZZAZIONE));
			
			parser = new CSVParserBuilder()
				    .withSeparator(';')
				    .withIgnoreQuotations(true)
				    .build();
	
		    csvReader = new CSVReaderBuilder(reader)
				    .withSkipLines(0)
				    .withCSVParser(parser)
				    .build();
		    
	
		    String[] line;
		    while ((line = csvReader.readNext()) != null)
		    	if (line[7].toLowerCase().equals(aux.toLowerCase()))
		    			ret = line[6];
		    
			   csvReader.close();
			   reader.close();
	    

	    return ret;  
		
	}
	
	public static void main(String[] argv) throws CsvValidationException, IOException
	{
		Transcoding t = new Transcoding();
		System.out.println(t.getUfficioFromAcronimo("USI"));
	}
}