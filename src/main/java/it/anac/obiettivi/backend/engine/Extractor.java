package it.anac.obiettivi.backend.engine;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opencsv.exceptions.CsvValidationException;

import it.anac.obiettivi.backend.model.report.Scheda;
import it.anac.obiettivi.backend.rest.RestConsts;
import it.anac.obiettivi.backend.rest.util.Mapping;

@RestController
@RequestMapping(value = { "/ws" })
public class Extractor extends RestConsts {
	
	private static String FILE = "pdf/output_"+System.currentTimeMillis()+".pdf";
    private static Font catFont = FontFactory.getFont("fonts/Titillium-Regular.otf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 18, Font.NORMAL, BaseColor.BLACK);
    private static Font subFont = FontFactory.getFont("fonts/Titillium-Regular.otf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 16, Font.NORMAL, BaseColor.BLACK);
    private static Font smallBold = FontFactory.getFont("fonts/Titillium-Regular.otf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD, BaseColor.WHITE);
    private static Font smallNormal = FontFactory.getFont("fonts/Titillium-Regular.otf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.NORMAL, BaseColor.BLACK);
    private static Font smallNormalw = FontFactory.getFont("fonts/Titillium-Regular.otf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.NORMAL, BaseColor.WHITE);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Value("${obiettivi.meseMonitoraggio}")
    private int meseMonitoraggio;
	
	private Scheda scheda;
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/report", method = RequestMethod.GET)	
	public ResponseEntity<InputStreamResource> download(
			String id, @RequestParam(defaultValue = "0") String obiettivi) throws ParseException, DocumentException, MalformedURLException, IOException, CsvValidationException {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(id));
		String json = mongoTemplate.findOne(query,String.class, "submissions");
		
	   /* byte[] encoded = Files.readAllBytes(Paths.get("C:\\omnia\\workspace-eclipse\\obiettivi-backend\\src\\main\\resources\\example2.json"));
		String json = new String(encoded);*/
		
        Document document = new Document();
    	document.setPageSize(PageSize.A4.rotate());
    	ReportHelper rh = new ReportHelper();
        int obb = rh.getObiettivi(json);
		
        PdfWriter.getInstance(document, new FileOutputStream(FILE));
        document.open();
        
        
    	if ("1".equalsIgnoreCase(obiettivi.trim()))
    	{
    		if(obb==2)
	        {
				scheda = rh.getSchedaFromJson_ob_1(json, meseMonitoraggio);
		    
		        addMetaData(document);
		        addTitlePage(document, "Monitoraggio");
		        addContent(document);
	        }
		    else
    		{
		        addTitlePage(document,true,"");		
    		}
    		
    	}
    	else if ("2".equalsIgnoreCase(obiettivi.trim()))
    	{
	        if(obb==2)
	        {
			    scheda = rh.getSchedaFromJson_ob_2(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document,  "Monitoraggio");
			    addContent(document);
	        }
	        else
    		{
		        addTitlePage(document,true,"");		
    		}
    	}
    	else if ("3".equalsIgnoreCase(obiettivi.trim()))
    	{
	        if(obb==3 || obb==5)
	        {
			    scheda = rh.getSchedaFromJson_ob_1bis(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document, "Nuovo Obiettivo");
			    addContent(document);
	        }
	        else
    		{
		        addTitlePage(document,true,"");		
    		}
    	}
    	else if ("4".equalsIgnoreCase(obiettivi.trim()))
    	{
	        if(obb==4 || obb==5)
	        {
			    scheda = rh.getSchedaFromJson_ob_2bis(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document,"Nuovo Obiettivo");
			    addContent(document);
	        }
	        else
    		{
		        addTitlePage(document,true,"");		
    		}
    	}
    	else
    	{
    		if(obb==1)
	        {
	    		scheda = rh.getSchedaFromJson_ob_1(json, meseMonitoraggio);
	    	    
		        addMetaData(document);
		        addTitlePage(document,"Monitoraggio");
		        addContent(document);
	        }		        
    		else if(obb==2)
	        {
                scheda = rh.getSchedaFromJson_ob_1(json, meseMonitoraggio);
	    	    
		        addMetaData(document);
		        addTitlePage(document,"Monitoraggio");
		        addContent(document);
		        
			    scheda = rh.getSchedaFromJson_ob_2(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document,"Monitoraggio");
			    addContent(document);
	        }
    		else if(obb==3)
	        {
    			scheda = rh.getSchedaFromJson_ob_1(json, meseMonitoraggio);
	    	    
		        addMetaData(document);
		        addTitlePage(document,"Monitoraggio");
		        addContent(document);
		        
			    scheda = rh.getSchedaFromJson_ob_1bis(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document,"Nuovo Obiettivo");
			    addContent(document);
			    
			    scheda = rh.getSchedaFromJson_ob_2(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document,"Monitoraggio");
			    addContent(document);
	        }
    		else if(obb==4)
	        {
                scheda = rh.getSchedaFromJson_ob_1(json, meseMonitoraggio);
	    	    
		        addMetaData(document);
		        addTitlePage(document,"Monitoraggio");
		        addContent(document);
		        
		        scheda = rh.getSchedaFromJson_ob_2(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document,"Monitoraggio");
			    addContent(document);
		        
			    scheda = rh.getSchedaFromJson_ob_2bis(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document,"Nuovo Obiettivo");
			    addContent(document);
	        }
    		else if(obb==5)
	        {
                scheda = rh.getSchedaFromJson_ob_1(json, meseMonitoraggio);
	    	    
		        addMetaData(document);
		        addTitlePage(document,"Monitoraggio");
		        addContent(document);
		        
		        scheda = rh.getSchedaFromJson_ob_1bis(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document,"Nuovo Obiettivo");
			    addContent(document);
			    
			    scheda = rh.getSchedaFromJson_ob_2(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document, "Monitoraggio");
			    addContent(document);
		        
			    scheda = rh.getSchedaFromJson_ob_2bis(json, meseMonitoraggio);
			    document.newPage();
			    addTitlePage(document,"Nuovo Obiettivo");
			    addContent(document);
	        }
    		else
    		{
		        addTitlePage(document,true,"");		
    		}
    		
    	}
	    
	    document.close();    	

	    InputStream inputStream = new FileInputStream(new File(FILE));
	    InputStreamResource inputStreamResource = new InputStreamResource(inputStream);
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_PDF);
	    headers.setContentLength(Files.size(Paths.get(FILE)));
	    
	    return new ResponseEntity(inputStreamResource, headers, HttpStatus.OK);
        
	}
	
	public void setScheda(Scheda scheda)
	{
		this.scheda = scheda;
	}
	
		
	private void addMetaData(Document document) {
        document.addTitle("Report Obiettivi 2022");
        document.addSubject(scheda.getNome()+" "+scheda.getCognome());
        document.addCreator("Sistema Automatico di generazione PDF - ANAC");
    }
	
	private void addTitlePage(Document document, String header)
			throws CsvValidationException, MalformedURLException, DocumentException, IOException
	{
		addTitlePage(document,false, header);
	}

    private void addTitlePage(Document document, boolean onlyHeader, String header)
            throws DocumentException, MalformedURLException, IOException, CsvValidationException {
        Paragraph prefazione = new Paragraph();
        
        Image image = Image.getInstance("anac.png");
        image.scaleToFit(400f,47.5f);
        image.setAlignment(Element.ALIGN_CENTER);
        document.add(image);
        
        addEmptyLine(prefazione, 1);
        Paragraph titolo = new Paragraph("Obiettivi ANAC 2022 - "+header, catFont);
        titolo.setAlignment(Element.ALIGN_CENTER);
        prefazione.add(titolo);

        addEmptyLine(prefazione, 1);
        
        if (!onlyHeader)
        {
	        prefazione.add(new Paragraph(
	                "Redattore: " +scheda.getNome()+" "+scheda.getCognome()+" ["+Mapping.getUfficioFromCode(scheda.getUfficio())+"]",
	                subFont));
        }
        
        addEmptyLine(prefazione, 1);

        document.add(prefazione);
    }

    private void addContent(Document document) throws DocumentException, CsvValidationException, IOException {
    	
    	BaseColor color = new BaseColor(31, 73, 125);
    	BaseColor color_2 = new BaseColor(141, 179, 226);
    	BaseColor black = new BaseColor(0,0,0);
    	  	
        PdfPTable table = new PdfPTable(new float[] { 25, 75 });
        table.setHeaderRows(1);

        /* START UFFICI */
        PdfPCell c_uffici = new PdfPCell(new Phrase("Ufficio/i",smallBold));
        c_uffici.setBackgroundColor(color);
        c_uffici.setHorizontalAlignment(Element.ALIGN_CENTER);
        c_uffici.setVerticalAlignment(Element.ALIGN_MIDDLE);
        c_uffici.setPaddingBottom(5);
        table.addCell(c_uffici);
        String uffici_coinvolti[] = scheda.getUffici();
        
        String out_uffici = "";
        if (uffici_coinvolti!=null)
        	out_uffici = Mapping.getUfficioFromCode(uffici_coinvolti[0]);
        for(int i=1;i<uffici_coinvolti.length; i++)
        	out_uffici = out_uffici + "\n" + Mapping.getUfficioFromCode(uffici_coinvolti[i]);    
        PdfPCell c_uffici_val = new PdfPCell(new Phrase(out_uffici,smallNormal));
        c_uffici_val.setHorizontalAlignment(Element.ALIGN_LEFT);
        c_uffici_val.setPaddingBottom(5);
        table.addCell(c_uffici_val);
        /* END UFFICI */
        
        /* Riga Vuota */  
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(" "));
        cell.setBackgroundColor(color_2);
        cell.setColspan(2);
        table.addCell(cell);

        /* START AREA STRATEGICA */
        PdfPCell c_areaStrategica = new PdfPCell(new Phrase("Area Strategica",smallBold));
        c_areaStrategica.setBackgroundColor(color);
        c_areaStrategica.setHorizontalAlignment(Element.ALIGN_CENTER);
        c_areaStrategica.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(c_areaStrategica);
        PdfPCell c_areaStrategica_val = new PdfPCell(new Phrase(scheda.getAreaStrategica(),smallNormal));
        c_areaStrategica_val.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c_areaStrategica_val);
        /* END AREA STRATEGICA */
        
        /* START OBIETTIVO STRATEGICO */
        PdfPCell c_obiettivoStrategico = new PdfPCell(new Phrase("Obiettivo Strategico",smallBold));
        c_obiettivoStrategico.setBackgroundColor(color);
        c_obiettivoStrategico.setHorizontalAlignment(Element.ALIGN_CENTER);
        c_obiettivoStrategico.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(c_obiettivoStrategico);
        PdfPCell c_obiettivoStrategico_val = new PdfPCell(new Phrase(scheda.getObiettivoStrategico(),smallNormal));
        c_obiettivoStrategico_val.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c_obiettivoStrategico_val);
        /* END OBIETTIVO STRATEGICO */
        
        /* START OBIETTIVO OPERATIVO / MIGLIORAMENTO */
        String titleObj = "";
        String objAux= "";
        if (scheda.getTipologiaObiettivo().trim().equals("1"))
        {
        	titleObj = "Obiettivo Operativo";
        	objAux = scheda.getObiettivoOperativo();
        }
        else
        {
        	titleObj = "Obiettivo Miglioramento";
        	objAux = scheda.getObiettivoMiglioramento();
        }
        
        PdfPCell c_obiettivoOperativo = new PdfPCell(new Phrase(titleObj,smallBold));
        c_obiettivoOperativo.setBackgroundColor(color);
        c_obiettivoOperativo.setHorizontalAlignment(Element.ALIGN_CENTER);
        c_obiettivoOperativo.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(c_obiettivoOperativo);
        PdfPCell c_obiettivoOperativo_val = new PdfPCell(new Phrase(objAux,smallNormal));
        c_obiettivoOperativo_val.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c_obiettivoOperativo_val);
        /* END OBIETTIVO OPERATIVO */
        
        /* START GRADO */
        PdfPCell c_grado = new PdfPCell();
        c_grado.setBackgroundColor(color);
        c_grado.setHorizontalAlignment(Element.ALIGN_CENTER);
        c_grado.setVerticalAlignment(Element.ALIGN_MIDDLE);
               
        PdfPTable t_grado_nested_t = new PdfPTable(1);
        PdfPCell c_grado_nested = new PdfPCell(new Phrase("Grado di significatività\n\n",smallBold));
        
        c_grado_nested.setHorizontalAlignment(Element.ALIGN_CENTER);
        c_grado_nested.setVerticalAlignment(Element.ALIGN_MIDDLE);
        c_grado_nested.setHorizontalAlignment(Element.ALIGN_CENTER);
        c_grado_nested.disableBorderSide(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
        t_grado_nested_t.addCell(c_grado_nested);
        
        c_grado_nested.setBackgroundColor(BaseColor.WHITE);
        c_grado_nested.setPhrase(new Phrase(scheda.getGradoDiSignificativita(),smallNormal));
        t_grado_nested_t.addCell(c_grado_nested);
        
        c_grado.addElement(t_grado_nested_t);
        
        table.addCell(c_grado);
        
        PdfPCell c_grado_val = new PdfPCell(new Phrase("- Rilevanza per il raggiungmento dell'obiettivo strategico: "+scheda.getRilevanza()
        		+ "\n- Complessità nella sua realizzazione: "+scheda.getComplessita(),smallNormal));
        c_grado_val.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c_grado_val);
        /* END GRADO */
      
        if (scheda.getObiettivo_raggiunto()!=null)
        {  	     	
	        cell = new PdfPCell(new Phrase("Stato dell'obiettivo\n(alla data di moniotraggio)",smallBold));
	        cell.setBackgroundColor(color);
	        cell.setPaddingBottom(5);
	        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        table.addCell(cell);
	        
        	String obiettivoRaggiuntoTxt = "";
        	if (scheda.getObiettivo_raggiunto().toLowerCase().trim().equals("in_corso"))
        		obiettivoRaggiuntoTxt = "Obiettivo in corso di raggiungimento";
        	if (scheda.getObiettivo_raggiunto().toLowerCase().trim().equals("non_raggiungibile"))
        		obiettivoRaggiuntoTxt = "Obiettivo non raggiungibile";
        	if (scheda.getObiettivo_raggiunto().toLowerCase().trim().equals("raggiunto"))
        		obiettivoRaggiuntoTxt = "Obiettivo raggiunto";
        			        
	        cell = new PdfPCell(new Phrase(obiettivoRaggiuntoTxt,smallNormal));
	        cell.setBackgroundColor(BaseColor.WHITE);
	        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	        table.addCell(cell);
	       
	        
	        if (scheda.getObiettivo_raggiunto().equals("non_raggiungibile"))
	        {
		        cell = new PdfPCell(new Phrase("NOTE",smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		        table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(scheda.getNote_raggiungibile(),smallNormal));
		        cell.setBackgroundColor(BaseColor.WHITE);
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        table.addCell(cell);
	        }
        }
        
        cell = new PdfPCell(new Phrase("In fase di pianificazione",smallBold));
        cell.setBackgroundColor(color_2);
        cell.setColspan(2);
        cell.setPaddingBottom(5);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("In fase di pianificazione",smallBold));
        cell.setBackgroundColor(color_2);
        cell.setColspan(2);
        
               
    	Paragraph p = new Paragraph();
    	table.setWidthPercentage(100);
    	p.add(table);
    	
    	
    	/**/
    	
    	PdfPTable template_table = new PdfPTable(new float[] {20,30,20,30});
    	cell = new PdfPCell(new Phrase("Tipologia di indicatore",smallBold));
        cell.setBackgroundColor(color_2);
        cell.setPaddingBottom(5);
        template_table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Algoritmo di calcolo",smallBold));
        cell.setBackgroundColor(color_2);
        cell.setPaddingBottom(5);
        template_table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Target %",smallBold));
        cell.setBackgroundColor(color_2);
        cell.setPaddingBottom(5);
        template_table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Significatività",smallBold));
        cell.setBackgroundColor(color_2);
        cell.setPaddingBottom(5);
        template_table.addCell(cell);
        
        
     /*   if (scheda.getTipologiaObiettivo().trim().equals("2"))
        {
        
	        if (!scheda.getIndicatore_prodotto().equals("0"))
	        {        
		    	cell = new PdfPCell(new Phrase(Mapping.getTemplateProdotto().get("indicatore"),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(Mapping.getTemplateProdotto().get("calcolo"),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(Mapping.getTemplateProdotto().get("target"),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(Mapping.getTemplateProdotto().get("significativita"),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
	        }
				        
		    	cell = new PdfPCell(new Phrase("Prodotto",smallNormal));
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(scheda.getCalcoloProdotto(),smallNormal));
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(scheda.getTargetProdotto(),smallNormal));
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(scheda.getDescrizioneProdotto(),smallNormal));
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
	        
	        if (!scheda.getIndicatore_efficacia().equals("0"))
	        {
		    	cell = new PdfPCell(new Phrase("EFFICACIA",smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(Mapping.getTemplateEfficacia().get("calcolo_"+scheda.getIndicatore_efficacia()),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(Mapping.getTemplateEfficacia().get("target_"+scheda.getIndicatore_efficacia()),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(Mapping.getTemplateEfficacia().get("significativita_"+scheda.getIndicatore_efficacia()),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
	        }
	        
	        
			    	cell = new PdfPCell(new Phrase("Efficacia\n\nAmbito di miglioramento misurato: "+scheda.getAmbito(),smallNormal));
			        cell.setPaddingBottom(5);
			        template_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase(scheda.getCalcoloEfficacia(),smallNormal));
			        cell.setPaddingBottom(5);
			        template_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase(scheda.getTargetEfficacia(),smallNormal));
			        cell.setPaddingBottom(5);
			        template_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase(scheda.getDescrizioneEfficacia(),smallNormal));
			        cell.setPaddingBottom(5);
			        template_table.addCell(cell);
	
			if (!scheda.getIndicatore_efficienza().equals("0"))
			 {
			        
		    	cell = new PdfPCell(new Phrase(Mapping.getTemplateEfficienza().get("indicatore"),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(Mapping.getTemplateEfficienza().get("calcolo"),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(Mapping.getTemplateEfficienza().get("target"),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(Mapping.getTemplateEfficienza().get("significativita"),smallBold));
		        cell.setBackgroundColor(color);
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
			 }
			        
			        
		    	cell = new PdfPCell(new Phrase("Efficienza",smallNormal));
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(scheda.getCalcoloEfficienza(),smallNormal));
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(scheda.getTargetEfficienza(),smallNormal));
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
		        
		        cell = new PdfPCell(new Phrase(scheda.getDescrizioneEfficienza(),smallNormal));
		        cell.setPaddingBottom(5);
		        template_table.addCell(cell);
	        
	        
	        template_table.setWidthPercentage(100);
	        p.add(template_table);
        }*/
    	
    	  	
      /*  if (scheda.getTipologiaObiettivo().trim().equals("1"))
        {*/
			    	/**/
			        PdfPTable pianificazione_table = new PdfPTable(new float[] {20,35,35,10});
			        cell = new PdfPCell(new Phrase("Indicatori di risultato",smallBold));
			        cell.setBackgroundColor(color);
			        cell.setColspan(1);
			        pianificazione_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase("Descrizione",smallNormal));
			        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			        pianificazione_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase("Metodo di calcolo",smallNormal));
			        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			        pianificazione_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase("Target %",smallNormal));
			        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			        pianificazione_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase("Indicatore di prodotto",smallBold));
			        cell.setBackgroundColor(color_2);
			        pianificazione_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase(scheda.getDescrizioneProdotto(),smallNormal));
			        pianificazione_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase(scheda.getCalcoloProdotto(),smallNormal));
			        pianificazione_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase(scheda.getTargetProdotto(),smallNormal));
			        pianificazione_table.addCell(cell);
			                
			        cell = new PdfPCell(new Phrase("Indicatore di efficacia",smallBold));
			        cell.setBackgroundColor(color_2);
			        pianificazione_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase(scheda.getDescrizioneEfficacia(),smallNormal));
			        pianificazione_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase(scheda.getCalcoloEfficacia(),smallNormal));
			        pianificazione_table.addCell(cell);
			        
			        cell = new PdfPCell(new Phrase(scheda.getTargetEfficienza()+"%",smallNormal));
			        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			        pianificazione_table.addCell(cell);
			 
			        cell = new PdfPCell(new Phrase("Indicatore di efficienza",smallBold));
			        cell.setBackgroundColor(color_2);
			        pianificazione_table.addCell(cell);
			        
			        pianificazione_table.addCell(new Phrase(scheda.getDescrizioneEfficienza(),smallNormal));
			        pianificazione_table.addCell(new Phrase(scheda.getCalcoloEfficienza(),smallNormal));
			        
			        cell = new PdfPCell(new Phrase(scheda.getTargetEfficacia()+"%",smallNormal));
			        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			        pianificazione_table.addCell(cell);
			        
			        pianificazione_table.setWidthPercentage(100);
			    	p.add(pianificazione_table);
      /*  } */
    	
    	PdfPTable date_table = new PdfPTable(new float[] {20,30,20,30});
    	cell = new PdfPCell(new Phrase("Data inizio",smallBold));
        cell.setBackgroundColor(color_2);
        cell.setPaddingBottom(5);
		date_table.addCell(cell);
        
        cell = new PdfPCell(new Phrase(scheda.getDataInizio(),smallNormalw));
        cell.setBackgroundColor(color);
        cell.setPaddingBottom(5);
        date_table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Data fine",smallBold));
        cell.setBackgroundColor(color_2);
        cell.setPaddingBottom(5);
        date_table.addCell(cell);
        
        cell = new PdfPCell(new Phrase(scheda.getDataFine(),smallNormalw));
        cell.setBackgroundColor(color);
        cell.setPaddingBottom(5);
        date_table.addCell(cell);
        
        date_table.setWidthPercentage(100);
        p.add(date_table);
        
    	PdfPTable responsabili_table = new PdfPTable(new float[] {25,75});
        cell = new PdfPCell(new Phrase("Responsabile/i",smallBold));
        cell.setBackgroundColor(color_2);
        responsabili_table.addCell(cell);
        
        String responsabili[] = scheda.getResponsabili();
        String out_responsabili = responsabili[0];
        for(int i=1;i<responsabili.length; i++)
        	out_responsabili = out_responsabili + "\n" + responsabili[i]; 
        
        cell = new PdfPCell(new Phrase(out_responsabili,smallNormal));
        responsabili_table.addCell(cell);
        
        responsabili_table.setWidthPercentage(100);
        p.add(responsabili_table);
    	
    	
    	p.setIndentationLeft(5);
    	p.setIndentationRight(5);
    	
    	document.add(p);
    	document.newPage();
    	
    	PdfPTable pea_header = new PdfPTable(new float[] {100});
        cell = new PdfPCell(new Phrase("Articolazione in fasi dell'obiettivo operativo/miglioramento - Piano esecutivo di azione (PEA)",smallBold));
        cell.setPaddingBottom(5);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color);
        pea_header.addCell(cell);
        pea_header.setWidthPercentage(100);
        
        
        PdfPTable pea_header_2 = new PdfPTable(new float[] {60,40});
        cell = new PdfPCell(new Phrase("FASE",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(5);
        cell.setBackgroundColor(color_2);
        pea_header_2.addCell(cell);
        cell = new PdfPCell(new Phrase("RISORSE UMANE",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(5);
        cell.setBackgroundColor(color_2);
        pea_header_2.addCell(cell);        
        pea_header_2.setWidthPercentage(100);

        
        PdfPTable pea_content_1 = new PdfPTable(new float[] {3,14,10,9,9,5,10,10,10,10,10});
        cell = new PdfPCell(new Phrase("N.",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Descrizione",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Responsabile",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Mese inizio",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Mese fine",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Peso fase",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Risultati attesi",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Categoria",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Nominativo",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Partecipazione alla fase (%)",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        cell = new PdfPCell(new Phrase("FTE di fase (%)",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color_2);
        pea_content_1.addCell(cell);
        
        
        /* SET PARAM PEA */        
        for (int i=0; i<scheda.getPea().length; i++)
        {
        	cell = new PdfPCell(new Phrase(""+(i+1),smallNormal));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setRowspan(scheda.getPea()[i].getRu_riga().length);            
            pea_content_1.addCell(cell);
            
            cell = new PdfPCell(new Phrase(scheda.getPea()[i].getDescrizione(),smallNormal));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setRowspan(scheda.getPea()[i].getRu_riga().length);            
            pea_content_1.addCell(cell);
            
            cell = new PdfPCell(new Phrase(scheda.getPea()[i].getResponsabile(),smallNormal));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setRowspan(scheda.getPea()[i].getRu_riga().length);            
            pea_content_1.addCell(cell);
            
            cell = new PdfPCell(new Phrase(scheda.getPea()[i].getMeseInizio(),smallNormal));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setRowspan(scheda.getPea()[i].getRu_riga().length);            
            pea_content_1.addCell(cell);
            
            cell = new PdfPCell(new Phrase(scheda.getPea()[i].getMeseFine(),smallNormal));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setRowspan(scheda.getPea()[i].getRu_riga().length);            
            pea_content_1.addCell(cell);
            
            cell = new PdfPCell(new Phrase(""+scheda.getPea()[i].getPesoFase(),smallNormal));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setRowspan(scheda.getPea()[i].getRu_riga().length);            
            pea_content_1.addCell(cell);
            
            cell = new PdfPCell(new Phrase(scheda.getPea()[i].getRisultatiAttesi(),smallNormal));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setRowspan(scheda.getPea()[i].getRu_riga().length);            
            pea_content_1.addCell(cell);
            
            for (int j=0; j<scheda.getPea()[i].getRu_riga().length; j++)   	            
            {
            	cell = new PdfPCell(new Phrase(scheda.getPea()[i].getRu_riga()[j].getCategoria(),smallNormal));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pea_content_1.addCell(cell);
                
                cell = new PdfPCell(new Phrase(scheda.getPea()[i].getRu_riga()[j].getNominativo(),smallNormal));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pea_content_1.addCell(cell);
     
                cell = new PdfPCell(new Phrase(""+scheda.getPea()[i].getRu_riga()[j].getPartecipazione(),smallNormal));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pea_content_1.addCell(cell);
                
                cell = new PdfPCell(new Phrase(""+scheda.getPea()[i].getRu_riga()[j].getFte(),smallNormal));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pea_content_1.addCell(cell);
            }            
             
            if (scheda.getObiettivo_raggiunto()!=null && !scheda.getObiettivo_raggiunto().equals("non_raggiungibile"))
            {    
	            PdfPTable monitoraggio = new PdfPTable(new float[] {25,75});
	            cell = new PdfPCell(new Phrase("% Raggiungimento della Fase",smallBold));
	            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	            cell.setPadding(5);
	            cell.setBackgroundColor(color_2);
	            monitoraggio.addCell(cell);
	            cell = new PdfPCell(new Phrase("Note",smallBold));
	            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	            cell.setPadding(5);
	            cell.setBackgroundColor(color_2);
	            
	            monitoraggio.addCell(cell);        
	            
	            cell = new PdfPCell(new Phrase(scheda.getPea()[i].getPercentualeMonitoraggio()+"%",smallNormal));
	            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	            cell.setPadding(5);
	            
	            monitoraggio.addCell(cell);        
	            
	            cell = new PdfPCell(new Phrase(scheda.getPea()[i].getNoteMonitoraggio(),smallNormal));
	            cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	            cell.setPadding(5);
	            
	            monitoraggio.addCell(cell);        
	            monitoraggio.setWidthPercentage(100);
	      
	            cell = new PdfPCell();
	            cell.setColspan(11);
	            cell.addElement(monitoraggio);
	            cell.setBorderColorBottom(BaseColor.BLUE);
	            cell.setBorderColorTop(BaseColor.BLUE);
	      
	            pea_content_1.addCell(cell);
            }
        }
               
        pea_content_1.setWidthPercentage(100);
 
    	Paragraph p2 = new Paragraph();
    	p2.add(pea_header);
    	p2.add(pea_header_2);
    	p2.add(pea_content_1);
    	
    	addEmptyLine(p2, 2);
    	
    	
    	/***/
    	
    	System.out.println("Obiettivo_raggiunto: "+scheda.getObiettivo_raggiunto());
    	
    	
    	
    	if (scheda.getObiettivo_raggiunto()!=null)
    		if (scheda.getObiettivo_raggiunto().trim().equals("raggiunto"))
    		{
    			System.out.println(scheda.getMonitoraggio().getProdottoLivelloDiRealizzazione());
    			    			
		    	PdfPTable rendicontazione = new PdfPTable(new float[] {100});
		        cell = new PdfPCell(new Phrase("Rendicontazione di monitoraggio",smallBold));
		        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell.setPaddingBottom(5);
		        cell.setBackgroundColor(color_2);
		        rendicontazione.addCell(cell);
		        rendicontazione.setWidthPercentage(100);
		    	p2.add(rendicontazione);
		    	
		    	rendicontazione = new PdfPTable(new float[] {20,40,40});
		    	
		        cell = new PdfPCell(new Phrase("Indicatore di prodotto",smallBold));
		        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell.setPaddingBottom(5);
		        cell.setBackgroundColor(color_2);
		        rendicontazione.addCell(cell);
		        cell = new PdfPCell(new Phrase("Livello di realizzazione: "+scheda.getMonitoraggio().getProdottoLivelloDiRealizzazione(),smallNormal));
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell.setPaddingBottom(5);
		        rendicontazione.addCell(cell);
		        cell = new PdfPCell(new Phrase("Note: "+scheda.getMonitoraggio().getProdottoNoteLivelloDiRealizzazione(),smallNormal));
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell.setPaddingBottom(5);
		        rendicontazione.addCell(cell);
		        rendicontazione.setWidthPercentage(100);
		        p2.add(rendicontazione);
		        
		    	rendicontazione = new PdfPTable(new float[] {20,25,25,15,15});
		        
		        cell = new PdfPCell(new Phrase("Indicatore di efficacia",smallBold));
		        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell.setPaddingBottom(5);
		        cell.setBackgroundColor(color_2);
		        rendicontazione.addCell(cell);
		        cell = new PdfPCell(new Phrase("Livello di realizzazione: "+scheda.getMonitoraggio().getEfficaciaLivelloDiRealizzazione(),smallNormal));
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell.setPaddingBottom(5);
		        rendicontazione.addCell(cell);
		        cell = new PdfPCell(new Phrase("Note: "+scheda.getMonitoraggio().getEfficaciaNoteLivelloDiRealizzazione(),smallNormal));
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell.setPaddingBottom(5);
		        rendicontazione.addCell(cell);
		        cell = new PdfPCell(new Phrase("Numeratore: "+scheda.getMonitoraggio().getEfficaciaNumeratore(),smallNormal));
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell.setPaddingBottom(5);
		        rendicontazione.addCell(cell);
		        cell = new PdfPCell(new Phrase("Denominatore: "+scheda.getMonitoraggio().getEfficaciaDenominatore(),smallNormal));
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell.setPaddingBottom(5);
		        rendicontazione.addCell(cell);
		        rendicontazione.setWidthPercentage(100);
		        
		        cell = new PdfPCell(new Phrase("Indicatore di efficienza",smallBold));
		        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell.setPaddingBottom(5);
		        cell.setBackgroundColor(color_2);
		        rendicontazione.addCell(cell);
		        cell = new PdfPCell(new Phrase("Livello di realizzazione: "+scheda.getMonitoraggio().getEfficienzaLivelloDiRealizzazione(),smallNormal));
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell.setPaddingBottom(5);
		        rendicontazione.addCell(cell);
		        cell = new PdfPCell(new Phrase("Note: "+scheda.getMonitoraggio().getEfficienzaNoteLivelloDiRealizzazione(),smallNormal));
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell.setPaddingBottom(5);
		        rendicontazione.addCell(cell);
		        cell = new PdfPCell(new Phrase("Mesi Utilizzati: "+scheda.getMonitoraggio().getEfficienzaMesiUtilizzati(),smallNormal));
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell.setPaddingBottom(5);
		        rendicontazione.addCell(cell);
		        cell = new PdfPCell(new Phrase("Mesi Utili: "+scheda.getMonitoraggio().getEfficienzaMesiUtili(),smallNormal));
		        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell.setPaddingBottom(5);
		        rendicontazione.addCell(cell);
		        rendicontazione.setWidthPercentage(100);
		        
		    	p2.add(rendicontazione);
    		}
    	
    	/***/
    	
    	addEmptyLine(p2, 2);  	
	
    	PdfPTable ru = new PdfPTable(new float[] {40,30,30});
        cell = new PdfPCell(new Phrase("Risorse Umane (FTE)",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingBottom(5);
        cell.setBackgroundColor(black);
        ru.addCell(cell);
        cell = new PdfPCell(new Phrase("Nominativo",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingBottom(5);
        cell.setBackgroundColor(black);
        ru.addCell(cell);
        cell = new PdfPCell(new Phrase("Coinvolgimento totale nell’obiettivo (FTE %)",smallBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingBottom(5);
        cell.setBackgroundColor(color_2);
        ru.addCell(cell);
        ru.setWidthPercentage(100);
    	p2.add(ru);
    	
    	PdfPTable ru_2 = new PdfPTable(new float[] {20,20,30,30});
        cell = new PdfPCell(new Phrase("Risorse Umane",smallBold));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(color);
        cell.setRowspan(scheda.getRu_tot().length);
        ru_2.addCell(cell);
             
        for(int i=0; i<scheda.getRu_tot().length; i++)
        {
	        cell = new PdfPCell(new Phrase(scheda.getRu_tot()[i].getCategoria(),smallBold));
	        cell.setBackgroundColor(color);
	        ru_2.addCell(cell);
	        
	        cell = new PdfPCell(new Phrase(scheda.getRu_tot()[i].getNominativo(),smallNormal));
	        ru_2.addCell(cell);
	        
	        cell = new PdfPCell(new Phrase(""+scheda.getRu_tot()[i].getFte(),smallNormal));
	        ru_2.addCell(cell);
        }
        
        ru_2.setWidthPercentage(100);
    	p2.add(ru_2);
        
    	p2.setIndentationLeft(5);
    	p2.setIndentationRight(5);
	    	
    	document.add(p2);
    }

    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

}
