package it.anac.obiettivi.backend.engine;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.anac.obiettivi.backend.model.report.Pea_riga;
import it.anac.obiettivi.backend.model.report.RendicontazioneDiMonitoraggio;
import it.anac.obiettivi.backend.model.report.Ru_riga;
import it.anac.obiettivi.backend.model.report.Ru_riga_tot;
import it.anac.obiettivi.backend.model.report.Scheda;
import it.anac.obiettivi.backend.rest.util.Mapping;

public class ReportHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportHelper.class);

	private String obiettivo = "";
	
	public int getObiettivi(String json) throws ParseException
	{
		JSONParser parser = new JSONParser();
		
		Object obj = parser.parse(json);
        JSONObject tot_jo = (JSONObject) obj;

        JSONObject jo = (JSONObject) tot_jo.get("data");
		
		long val = (long) jo.get("obiettivi_da_compilare");
		@SuppressWarnings("deprecation")
		Long res = new Long(val);
		int ret = res.intValue(); // restituisce 1 o 2
				
		String ob_1bis = (String) jo.get("obiettivo_raggiunto");
		boolean nuovo_obiettivo_1 = false;
		if (jo.get("inserisci_nuovo_obiettivo_1")!=null)
			nuovo_obiettivo_1 = (boolean) jo.get("inserisci_nuovo_obiettivo_1");
		if (ob_1bis!=null)
			if (ob_1bis.equals("non_raggiungibile") && (nuovo_obiettivo_1))
				ret = 3;
				
		String ob_2bis = (String) jo.get("obiettivo_raggiunto2");
		boolean nuovo_obiettivo_2 = false;
		if (jo.get("inserisci_nuovo_obiettivo_2")!=null)
			nuovo_obiettivo_2 = (boolean) jo.get("inserisci_nuovo_obiettivo_2");
		if (ob_2bis!=null)
			if (ob_2bis.equals("non_raggiungibile") && (nuovo_obiettivo_2))
			{
				if (ret==3) ret = 5; else ret = 4;
			}
				
		return ret;
	}
	
	public Scheda getSchedaFromJson_ob_1(String json, int meseMonitoraggio) throws ParseException
	{
		this.obiettivo="";
		return getSchedaFromJson(json, meseMonitoraggio);
	}
	
	public Scheda getSchedaFromJson_ob_1bis(String json,  int meseMonitoraggio) throws ParseException
	{
		this.obiettivo="3";
		return getSchedaFromJson(json, meseMonitoraggio);
	}
	
	public Scheda getSchedaFromJson_ob_2(String json, int meseMonitoraggio) throws ParseException
	{
		this.obiettivo="2";
		return getSchedaFromJson(json, meseMonitoraggio);
	}
	
	public Scheda getSchedaFromJson_ob_2bis(String json, int meseMonitoraggio) throws ParseException
	{
		this.obiettivo="4";
		return getSchedaFromJson(json, meseMonitoraggio);
	}
	
	private String getValFromJson(String json, String val, String def) throws ParseException
	{
	    JSONParser parser = new JSONParser();
	   	
		Object obj = parser.parse(json);
	    JSONObject tot_jo = (JSONObject) obj;

	    JSONObject jo = (JSONObject) tot_jo.get("data");
	        
		String aux = (String)jo.get(val);
		if (aux!=null)
			return aux;
		else
			return def;
	}
	
	public Scheda getSchedaFromJson(String json, int meseMonitoraggio) throws ParseException
	{
        JSONParser parser = new JSONParser();
		Object obj = parser.parse(json);
        JSONObject tot_jo = (JSONObject) obj;
        
        JSONObject jo = (JSONObject) tot_jo.get("data");
		
		Scheda scheda = new Scheda();
		scheda.setNome((String)jo.get("nome"));
		scheda.setCognome((String)jo.get("cognome"));
		scheda.setUfficio((String)jo.get("ufficio"));
		
		scheda.setObiettivo_raggiunto((String)jo.get("obiettivo_raggiunto"+obiettivo));
		if (scheda.getObiettivo_raggiunto()!=null)
			if (scheda.getObiettivo_raggiunto().equals("non_raggiungibile"))
			{
				scheda.setNote_raggiungibile((String)jo.get("note_raggiungibile"+obiettivo));
			}
		
		RendicontazioneDiMonitoraggio rendicontazione = new RendicontazioneDiMonitoraggio();
		rendicontazione.setProdottoLivelloDiRealizzazione((String)jo.get("prodotto_realizzazione"+obiettivo));
		rendicontazione.setProdottoNoteLivelloDiRealizzazione((String)jo.get("prodotto_realizzazione_note"+obiettivo));
		rendicontazione.setEfficaciaLivelloDiRealizzazione((String)jo.get("efficacia_realizzazione"+obiettivo));
		rendicontazione.setEfficaciaNoteLivelloDiRealizzazione((String)jo.get("efficacia_realizzazione_note"+obiettivo));
		
		
				
		if (jo.get("efficienza_realizzazione_mesi_utilizzati"+obiettivo)!=null && !jo.get("efficienza_realizzazione_mesi_utilizzati"+obiettivo).equals(""))
			rendicontazione.setEfficienzaMesiUtilizzati(Integer.valueOf(""+jo.get("efficienza_realizzazione_mesi_utilizzati"+obiettivo)));

		if (jo.get("efficienza_realizzazione_mesi_utili"+obiettivo)!=null && !jo.get("efficienza_realizzazione_mesi_utili"+obiettivo).equals(""))
			rendicontazione.setEfficienzaMesiUtili(Integer.valueOf(""+jo.get("efficienza_realizzazione_mesi_utili"+obiettivo)));

		
		rendicontazione.setEfficienzaLivelloDiRealizzazione((String)jo.get("efficienza_realizzazione"+obiettivo));
		rendicontazione.setEfficienzaNoteLivelloDiRealizzazione((String)jo.get("efficienza_realizzazione_note"+obiettivo));
		rendicontazione.setEfficaciaNumeratore((String)jo.get("efficacia_realizzazione_numeratore"+obiettivo));
		rendicontazione.setEfficaciaDenominatore((String)jo.get("efficacia_realizzazione_denominatore"+obiettivo));

		scheda.setMonitoraggio(rendicontazione);
				
		JSONArray uffici_coinvolti = (JSONArray) jo.get("uffici_coinvolti"+obiettivo);
		String uffici[] = new String[uffici_coinvolti.size()];
		for (int i=0; i<uffici_coinvolti.size(); i++)
			uffici[i]= (String)uffici_coinvolti.get(i);
		scheda.setUffici(uffici);
		
		String tipologiaObiettivo = ""+(Long)jo.get("tipologiaObiettivo"+obiettivo);
		scheda.setTipologiaObiettivo(tipologiaObiettivo.trim());
		
		
		if (scheda.getTipologiaObiettivo().trim().equals("2"))
		{
			scheda.setAreaStrategica("-");
			scheda.setObiettivoStrategico("-");
		}
		else
		{
			scheda.setAreaStrategica(Mapping.getAreaStrategica(getValFromJson(json,"area_strategica"+obiettivo,"-")));
			LOGGER.info("AREA STRATEGICA: "+scheda.getAreaStrategica());
			
			String os = "";
			String as = getValFromJson(json,"area_strategica"+obiettivo,"-");
			
			LOGGER.info("AREA STRATEGICA codifica: "+as);
			
			if (as.trim().equals("anticorruzione"))		
			{
				if (jo.get("obiettivo_strategico_1"+obiettivo)!=null)
					os = "1_"+(long)jo.get("obiettivo_strategico_1"+obiettivo);
				LOGGER.info("obiettivo_strategico_1"+obiettivo+" : "+os);
			}

			if (as.trim().equals("contratti"))		
			{
				if (jo.get("obiettivo_strategico_2"+obiettivo)!=null)
					os = "2_"+(long)jo.get("obiettivo_strategico_2"+obiettivo);
				LOGGER.info("obiettivo_strategico_2"+obiettivo+" : "+os);
			}
			
			if (as.trim().equals("supporto"))
			{
				if (jo.get("obiettivo_strategico_3"+obiettivo)!=null)
					os = "3_"+(long)jo.get("obiettivo_strategico_3"+obiettivo);
				LOGGER.info("obiettivo_strategico_3"+obiettivo+" : "+os);
			}
			
			scheda.setObiettivoStrategico(Mapping.getObiettivoStrategico(os));
			LOGGER.info("OBIETTIVO STRATEGICO: "+scheda.getObiettivoStrategico());
		}
		
		String obiettivo_migl_op = "";
		if (scheda.getTipologiaObiettivo().trim().equals("1"))
		{
			//                                       obiettivo_operativo2
			obiettivo_migl_op = getValFromJson(json,"obiettivo_operativo"+obiettivo,"-");
			scheda.setObiettivoOperativo(obiettivo_migl_op);
		}
		else
		{
			obiettivo_migl_op = getValFromJson(json,"obiettivo_miglioramento"+obiettivo,"-");
			scheda.setObiettivoMiglioramento(obiettivo_migl_op);
		}
	
		scheda.setGradoDiSignificativita((String)jo.get("grado"+obiettivo));
		
		if (jo.get("rilevanza"+obiettivo)!=null)
			scheda.setRilevanza(""+(long)jo.get("rilevanza"+obiettivo));
		else
			scheda.setRilevanza(""+(long)jo.get("rilevanza_bis"+obiettivo));
		
		scheda.setComplessita(""+(long)jo.get("complessita"+obiettivo));
		
		
		
		/**/

		scheda.setIndicatore_efficienza("0");
		scheda.setIndicatore_prodotto("0");
		scheda.setIndicatore_efficacia("0");
		
		
		if ((""+(Long)jo.get("tipologiaObiettivo"+obiettivo)).trim().equals("1"))
		{
			scheda.setDescrizioneProdotto((String)jo.get("descrizione_prodotto"+obiettivo));
			scheda.setDescrizioneEfficacia((String)jo.get("descrizionie_efficacia"+obiettivo));
			scheda.setDescrizioneEfficienza((String)jo.get("descrizione_efficienza"+obiettivo));
			
			scheda.setCalcoloProdotto((String)jo.get("calcolo_prodotto"+obiettivo));

			
			scheda.setCalcoloEfficacia((String)jo.get("calcolo_efficacia"+obiettivo));
			scheda.setCalcoloEfficienza((String)jo.get("calcolo_efficienza"+obiettivo));
			
			scheda.setTargetProdotto((String)jo.get("target_prodotto"+obiettivo));
			
			String min = ""+(Long)jo.get("target_efficacia_min"+obiettivo);
			String max = ""+(Long)jo.get("target_efficacia_max"+obiettivo);
			scheda.setTargetEfficacia("Min: "+min+"%\nMax:"+max);
			
			scheda.setTargetEfficienza(""+Long.parseLong(""+jo.get("target_efficienza"+obiettivo)));
		}
		else if ((""+(Long)jo.get("tipologiaObiettivo"+obiettivo)).trim().equals("2"))
		{
			scheda.setDescrizioneProdotto((String)jo.get("descrizione_prodotto_bis"+obiettivo));
			scheda.setDescrizioneEfficacia((String)jo.get("descrizionie_efficacia_bis"+obiettivo));
			scheda.setDescrizioneEfficienza((String)jo.get("descrizione_efficienza_bis"+obiettivo));
			
			scheda.setCalcoloProdotto((String)jo.get("calcolo_prodotto_bis"+obiettivo));
			
			scheda.setCalcoloEfficacia((String)jo.get("calcolo_efficacia_bis"+obiettivo));
			scheda.setCalcoloEfficienza((String)jo.get("calcolo_efficienza_bis"+obiettivo));
			
			scheda.setTargetProdotto((String)jo.get("target_prodotto_bis"+obiettivo));
			
			String min = ""+(Long)jo.get("target_efficacia_min_bis"+obiettivo);
			String max = ""+(Long)jo.get("target_efficacia_max_bis"+obiettivo);
			scheda.setTargetEfficacia("Min: "+min+"%\nMax:"+max);
			
			scheda.setTargetEfficienza(""+Long.parseLong(""+jo.get("target_efficienza_bis"+obiettivo)));
		}
		
		
		scheda.setDataInizio(Mapping.getMeseStartEnd((String)jo.get("data_inizio"+obiettivo)));
		scheda.setDataFine(Mapping.getMeseStartEnd((String)jo.get("data_fine"+obiettivo)));
		
		JSONArray responsabili = (JSONArray) jo.get("responsabili"+obiettivo);
		String responsabili_[] = new String[responsabili.size()];
		for (int i=0; i<responsabili.size(); i++)
			responsabili_[i]= (String)((JSONObject)responsabili.get(i)).get("nome");
		scheda.setResponsabili(responsabili_);		

		HashSet<String> dirigenti_ru = new HashSet<String>();
		HashSet<String> funzionari_ru = new HashSet<String>();
		HashSet<String> impiegati_ru = new HashSet<String>();
		
		HashMap<String, Double> fte_tot = new HashMap<String, Double>();
	
		JSONArray fasi = (JSONArray) jo.get("fasi"+obiettivo);	
		Pea_riga[] pea_righe = new Pea_riga[fasi.size()];
		for (int i=0; i<fasi.size(); i++)
		{
			Pea_riga pea_riga = new Pea_riga();
			pea_riga.setDescrizione((String)((JSONObject)fasi.get(i)).get("descrizione_fase"+obiettivo));
			pea_riga.setResponsabile((String)((JSONObject)((JSONObject)fasi.get(i)).get("responsabile"+obiettivo)).get("nome"));
			
			pea_riga.setMeseInizio(Mapping.getMese(""+(long)((JSONObject)fasi.get(i)).get("mese_inizio"+obiettivo)));
			pea_riga.setMeseFine(Mapping.getMese(""+(long)((JSONObject)fasi.get(i)).get("mese_fine"+obiettivo)));

			pea_riga.setPesoFase((Integer.parseInt(""+((JSONObject)fasi.get(i)).get("peso_fase"+obiettivo))));
			pea_riga.setRisultatiAttesi((String)((JSONObject)fasi.get(i)).get("risultati_attesi"+obiettivo));

			/************SOLO MONITORAGGIO**********/
			// note_monitoraggio
			// percentuale_monitoraggio
			// da settare solo se in monitoraggio
			String obiettivoRaggiuntoAux = (String)((JSONObject)jo).get("obiettivo_raggiunto"+obiettivo);
			if (obiettivoRaggiuntoAux!=null)
				if (!obiettivoRaggiuntoAux.equals("non_raggiungibile"))
				{
					String percentuale_monitoraggio = "0";
					if ((long)((JSONObject)fasi.get(i)).get("mese_inizio"+obiettivo)<=meseMonitoraggio)
						if (((JSONObject)fasi.get(i)).get("percentuale_monitoraggio"+obiettivo)!=null)
							percentuale_monitoraggio = ""+(Integer.parseInt(""+((JSONObject)fasi.get(i)).get("percentuale_monitoraggio"+obiettivo)));
					if ((long)((JSONObject)fasi.get(i)).get("mese_inizio"+obiettivo)>meseMonitoraggio)
						if (((JSONObject)fasi.get(i)).get("percentuale_monitoraggio_bis"+obiettivo)!=null)
							percentuale_monitoraggio = (""+((JSONObject)fasi.get(i)).get("percentuale_monitoraggio_bis"+obiettivo));
					
					if (percentuale_monitoraggio.trim().equals(""))
						percentuale_monitoraggio = "N.D.";
					
					pea_riga.setPercentualeMonitoraggio(percentuale_monitoraggio);
					
					String note_monitoraggio = "N.D.";
					if ((long)((JSONObject)fasi.get(i)).get("mese_inizio"+obiettivo)<=10)
						if (((JSONObject)fasi.get(i)).get("note_monitoraggio"+obiettivo)!=null)
							note_monitoraggio = (String)((JSONObject)fasi.get(i)).get("note_monitoraggio"+obiettivo);
					if ((long)((JSONObject)fasi.get(i)).get("mese_inizio"+obiettivo)>10)
						if (((JSONObject)fasi.get(i)).get("note_monitoraggio_bis"+obiettivo)!=null) 
							note_monitoraggio = (String)((JSONObject)fasi.get(i)).get("note_monitoraggio_bis"+obiettivo);
					
					if (note_monitoraggio.trim().equals(""))
						note_monitoraggio = "N.D.";
					
					pea_riga.setNoteMonitoraggio(note_monitoraggio);
					
					LOGGER.info("PERCENTUALE_MONITORAGGIO: "+pea_riga.getPercentualeMonitoraggio());
					LOGGER.info("NOTE_MONITORAGGIO: "+pea_riga.getNoteMonitoraggio());
				}
			/***************************************/
	
			JSONArray ru_array = (JSONArray) ((JSONObject)fasi.get(i)).get("risorse_umane"+obiettivo);
			
			long diff_l = (long)((JSONObject)fasi.get(i)).get("mese_fine"+obiettivo)+1 - (long)((JSONObject)fasi.get(i)).get("mese_inizio"+obiettivo);
	
			@SuppressWarnings("deprecation")
			int diff = (new Long(diff_l)).intValue();
			Ru_riga[] ru_righe = new Ru_riga[ru_array.size()];
			for (int j=0; j<ru_righe.length; j++)
			{
				Ru_riga ru_riga = new Ru_riga();
				
				ru_riga.setCategoria(Mapping.getCategoria((String)((JSONObject)ru_array.get(j)).get("categoria"+obiettivo)));
				ru_riga.setNominativo((String)((JSONObject)((JSONObject)ru_array.get(j)).get("nominativo"+obiettivo)).get("nome"));
				
				
		        if (((String)((JSONObject)ru_array.get(j)).get("categoria"+obiettivo)).equalsIgnoreCase("dirigente"))
		        	dirigenti_ru.add(ru_riga.getNominativo());
		        else if (((String)((JSONObject)ru_array.get(j)).get("categoria"+obiettivo)).equalsIgnoreCase("funzionario"))
		        	funzionari_ru.add(ru_riga.getNominativo());
		        else if (((String)((JSONObject)ru_array.get(j)).get("categoria"+obiettivo)).equalsIgnoreCase("impiegato"))
		        	impiegati_ru.add(ru_riga.getNominativo());
				
				String aux = ""+((JSONObject)ru_array.get(j)).get("partecipazione_fase"+obiettivo);	
				ru_riga.setPartecipazione(Double.parseDouble(aux));
				
				double fte_aux = Double.parseDouble(""+((JSONObject)ru_array.get(j)).get("partecipazione_fase"+obiettivo));								
				double val = (diff*20d)/240d;
				val = BigDecimal.valueOf(fte_aux*val).setScale(3, RoundingMode.HALF_UP).doubleValue();
								
				ru_riga.setFte(val);
				
				ru_riga.getNominativo();

				double aux_fte = 0;
				if (fte_tot.get(ru_riga.getNominativo())!=null)
					aux_fte = fte_tot.get(ru_riga.getNominativo());
		        fte_tot.put(ru_riga.getNominativo(), aux_fte+val);
				
				ru_righe[j] = ru_riga;
			}
			
			pea_riga.setRu_riga(ru_righe);
			pea_righe[i] = pea_riga;
		}
		
		scheda.setPea(pea_righe);
		
		
		Ru_riga_tot[] ru_tot = new Ru_riga_tot[dirigenti_ru.size()+
		                                       funzionari_ru.size()+
		                                       impiegati_ru.size()];
		
		Object[] dirigenti = dirigenti_ru.toArray();
		Arrays.sort(dirigenti);
		for (int i=0; i<dirigenti.length; i++)
		{
			Ru_riga_tot tot = new Ru_riga_tot();
			tot.setNominativo((String)dirigenti[i]);
			if (dirigenti.length>1)
				tot.setCategoria("Dirigente "+(i+1));
			else
				tot.setCategoria("Dirigente");
					
			tot.setFte(BigDecimal.valueOf(fte_tot.get(tot.getNominativo())).setScale(3, RoundingMode.HALF_UP).doubleValue());
			
			ru_tot[i]=tot;
		}
		
		Object[] funzionari =  funzionari_ru.toArray();
		Arrays.sort(funzionari);
		for (int i=0; i<funzionari.length; i++)
		{
			Ru_riga_tot tot = new Ru_riga_tot();
			tot.setNominativo((String)funzionari[i]);
			if (funzionari.length>1)
				tot.setCategoria("Funzionario "+(i+1));
			else
				tot.setCategoria("Funzionario");
			
			tot.setFte(BigDecimal.valueOf(fte_tot.get(tot.getNominativo())).setScale(3, RoundingMode.HALF_UP).doubleValue());
			
			ru_tot[i+dirigenti.length]=tot;
		}
		
		Object[] impiegati =  impiegati_ru.toArray();
		Arrays.sort(impiegati);
		for (int i=0; i<impiegati.length; i++)
		{
			Ru_riga_tot tot = new Ru_riga_tot();
			tot.setNominativo((String)impiegati[i]);
			if (impiegati.length>1)
				tot.setCategoria("Impiegato "+(i+1));
			else
				tot.setCategoria("Impiegato");
					
			tot.setFte(BigDecimal.valueOf(fte_tot.get(tot.getNominativo())).setScale(3, RoundingMode.HALF_UP).doubleValue());
			
			ru_tot[i+dirigenti.length+funzionari.length]=tot;
		}
		
		scheda.setRu_tot(ru_tot);
		
		return scheda;
	}
}
