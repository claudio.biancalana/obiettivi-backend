package it.anac.obiettivi.backend.rest.util;

import java.io.IOException;
import java.util.HashMap;

import com.opencsv.exceptions.CsvValidationException;

import it.anac.obiettivi.backend.engine.Transcoding;

public class Mapping {
		
	
	public static String getUfficioFromCode(String val) throws CsvValidationException, IOException
	{
		Transcoding transcoding = new Transcoding();
		return transcoding.getUfficioFromCode(val);
		
	}
	
	public static String getObiettivoStrategico(String code)
	{
		if (code.trim().equals("1_1"))
			return "OBIETTIVO STRATEGICO 1.1 - Realizzazione della piattaforma unica della trasparenza amministrativa";
		else if (code.trim().equals("2_1"))
			return "OBIETTIVO STRATEGICO 2.1 - Istituzione fascicolo virtuale degli operatori economici";
		else if (code.trim().equals("2_2"))
			return "OBIETTIVO STRATEGICO 2.2 - Potenziamento della Banca Dati Nazionale dei Contratti Pubblici (BDNCP)";
		else if (code.trim().equals("3_1"))
			return "OBIETTIVO STRATEGICO 3.1 - Progettazione ed implementazione di un Sistema di Controllo di Gestione";
		else if (code.trim().equals("3_2"))
			return "OBIETTIVO STRATEGICO 3.2 - Altro";

		return "-";
	}
	
	public static String getAreaStrategica(String code)
	{
		if (code.trim().equals("anticorruzione"))
			return "Anticorruzione e trasparenza";
		else if (code.trim().equals("contratti"))
			return "Contratti pubblici";
		else if (code.trim().equals("supporto"))
			return "Supporto, comunicazione e gestione";

		return code;
	}
	
	public static String getMese(String code)
	{
		String ret = "";
		
		if(code.trim().equals("1"))
			return "Gennaio 2022";
		if(code.trim().equals("2"))
			return "Febbraio 2022";
		if(code.trim().equals("3"))
			return "Marzo 2022";
		if(code.trim().equals("4"))
			return "Aprile 2022";
		if(code.trim().equals("5"))
			return "Maggio 2022";
		if(code.trim().equals("6"))
			return "Giugno 2022";
		if(code.trim().equals("7"))
			return "Luglio 2022";
		if(code.trim().equals("8"))
			return "Agosto 2022";
		if(code.trim().equals("9"))
			return "Settembre 2022";
		if(code.trim().equals("10"))
			return "Ottobre 2022";
		if(code.trim().equals("11"))
			return "Novembre 2022";
		if(code.trim().equals("12"))
			return "Dicembre 2022";
		
		return ret;
	}
	
	public static String getCategoria(String code)
	{
		String ret = "";
		
		if (code.trim().equalsIgnoreCase("dirigente"))
			return "Dirigente";
		if (code.trim().equalsIgnoreCase("funzionario"))
			return "Funzionario";
		if (code.trim().equalsIgnoreCase("impiegato"))
			return "Impiegato";
		
		return ret;
	}
	
	public static String getMeseStartEnd(String code)
	{
		String ret = "";
		
		if(code.trim().equals("gennaio2022"))
			return "Gennaio 2022";
		if(code.trim().equals("febbraio2022"))
			return "Febbraio 2022";
		if(code.trim().equals("marzo2022"))
			return "Marzo 2022";
		if(code.trim().equals("aprile2022"))
			return "Aprile 2022";
		if(code.trim().equals("maggio2022"))
			return "Maggio 2022";
		if(code.trim().equals("giugno2022"))
			return "Giugno 2022";
		if(code.trim().equals("luglio2022"))
			return "Luglio 2022";
		if(code.trim().equals("agosto2022"))
			return "Agosto 2022";
		if(code.trim().equals("settembre2022"))
			return "Settembre 2022";
		if(code.trim().equals("ottobre2022"))
			return "Ottobre 2022";
		if(code.trim().equals("novembre2022"))
			return "Novembre 2022";
		if(code.trim().equals("dicembre2022"))
			return "Dicembre 2022";
		
		return ret;
	}
	
	public static HashMap<String,String> getTemplateProdotto()
	{
		
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("indicatore","PRODOTTO");
		
		hm.put("calcolo", "E’ UN INDICATORE DEFINITO TECNICAMENTE «ON-OFF» (SI DEVE DESCRIVERE CON CHIAREZZA IL PRODOTTO ATTESO)");
		hm.put("target", "IL TARGET E’ CONSIDERATO RAGGIUNTO SE IL PRODOTTO E’ STATO REALIZZATO SECONDO LE CARATTERISTICHE DEFINITE IN SEDE DI PIANIFICAZIONE");
		hm.put("significativita", "L’INDICATORE CONSENTE DI VERIFICARE E VALUTARE LE CARATTERISTICHE DEL PRODOTTO REALIZZATO CHE CONSENTE IL RAGGIUNGIMENTO DELL’OBIETTIVO OPERATIVO");
		
		return hm;		
	}
	
	public static HashMap<String,String> getTemplateEfficienza()
	{
		
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("indicatore","EFFICIENZA");
		
		hm.put("calcolo", "N. DI MESI IMPIEGATI PER IL RAGGIUNGIMENTO DELL’OBIETTIVO OPERATIVO\r\n" + 
				"\r\n" + 
				"DIVISO\r\n" + 
				"\r\n" + 
				"N. DI MESI PIANIFICATI");
		hm.put("target", "IL TARGET E’ ESPRESSO IN VALORI PERCENUTALI. LA PERCENTUALE PROSSIMA AL 100% INDICA CHE I TEMPI DI REALIZZAZIONE DEL PROGETTO RISPETTANO I TEMPI PIANIFICATI");
		hm.put("significativita", "INDICATORE PROPOSTO PER TUTTI GLI OBIETTIVI OPERATIVI. CONSENTE DI MISURARE E VALUTARE A) LA CAPACITA’ DI PIANIFICAZIONE; B) L’EFFETTIVA CAPACITA’ DI RISPETTARE I TEMPI PIANIFICATI");
		
		return hm;		
	}
	
	public static HashMap<String,String> getTemplateEfficacia()
	{
		
		HashMap<String, String> hm = new HashMap<String, String>();
		
		hm.put("ambito_1","PRODUTTIVITA'");
		hm.put("calcolo_1", "Totale prodotti realizzati nell’anno (n) – Totale prodotti realizzati nell’anno precedente (n-1)\r\n" + 
				"\r\n" + 
				"DIVISO\r\n" + 
				"\r\n" + 
				"Totale prodotti realizzati nell’anno (n) + totale prodotti realizzati nell’anno precedente (n-1)");
		hm.put("target_1", "IL TARGET E’ ESPRESSO IN VALORI PERCENTUALI. PER AVERE IL DOPPIO DI PRODUTTIVITA’ LA PERCENTUALE DEVE ESSERE SUPERIORE AL 33%");
		hm.put("significativita_1", "L’INDICATORE MISURA IL MIGLIORAMENTO DELLA PRODUTTIVITA’ DEFINITO COME AUMENTO DEI «PRODOTTI» REALIZZATI NELL’ANNO RISPETTO A QUELLI REALIZZATI NELL’ANNO PRECEDENTE");
		
		hm.put("ambito_2","PRODUTTIVITA'");
		hm.put("calcolo_2", "Totale arretrato accumulato nell’anno precedente (n-1)  – Totale arretrato accumulato nell’anno (n)\r\n" + 
				"\r\n" + 
				"DIVISO\r\n" + 
				"\r\n" + 
				"Totale arretrato accumulato nell’anno precedente (n-1)  + Totale arretrato accumulato nell’anno (n)");
		hm.put("target_2", "IL TARGET E’ ESPRESSO IN VALORI PERCENTUALI. PER RIDURRE L’ARRETRATO DI ALMENO LA META’ LA PERC DEVE ESSERE SUPERIORE AL 33% . CON IL 100% L’ARRETRATO E’ AZZERATO");
		hm.put("significativita_2", "L’INDICATORE MISURA IL MIGLIORAMENTO IN TERMINI DI RIDUZIONE FINO ALL’AZZERAMENTO DELL’ARRETRATO");
		
		hm.put("ambito_3","COSTO");
		hm.put("calcolo_3", "Costo medio per prodotto anno (n-1) – costo medio prodotto  anno (n)\r\n" + 
				"\r\n" + 
				"DIVISO\r\n" + 
				"\r\n" + 
				"Costo medio per prodotto anno (n-1) + Costo medio per prodotto anno n");
		hm.put("target_3", "IL TARGET E’ ESPRESSO IN VALORI PERCENTUALI. PER AVERE LA META’ DEL COSTO MEDIO PER PRODOTTO LA PERC DEVE ESSERE SUPERIORE AL 33%");
		hm.put("significativita_3", "L’INDICATORE MISURA IL MIGLIORAMENTO DEI COSTI DEFINITO COME DIMINUIZIONE DEL COSTO MEDIO DEI «PRODOTTI» REALIZZATI NELL’ANNO RISPETTO A QUELLI REALIZZATI NELL’ANNO PRECEDENTE");
		
		hm.put("ambito_4","COSTO");
		hm.put("calcolo_4", "Costo medio per processo anno (n-1) – costo medio processo  anno (n)\r\n" + 
				"\r\n" + 
				"DIVISO\r\n" + 
				"\r\n" + 
				"Costo medio per processo anno (n-1) + Costo medio processo anno n");
		hm.put("target_4", "IL TARGET E’ ESPRESSO IN VALORI PERCENTUALI. PER AVERE LA META’ DEL COSTO MEDIO PER PROCESSO LA PERC DEVE ESSERE SUPERIORE AL 33%");
		hm.put("significativita_4", "L’INDICATORE MISURA IL MIGLIORAMENTO DEI COSTI DEFINITO COME DIMINUIZIONE DEL COSTO MEDIO DEI «PROCESSI» REALIZZATI NELL’ANNO RISPETTO A QUELLI REALIZZATI NELL’ANNO PRECEDENTE");
		
		hm.put("ambito_5","TEMPI");
		hm.put("calcolo_5", "Tempo medio prodotto anno (n-1) – tempo medio prodotto  anno (n)\r\n" + 
				"\r\n" + 
				"DIVISO\r\n" + 
				"\r\n" + 
				"Tempo  medio prodotto anno (n-1) + Tempo medio prodotto anno n");
		hm.put("target_5", "IL TARGET E’ ESPRESSO IN VALORI PERCENTUALI. PER AVERE LA META’ DEL TEMPO MEDIO PER PRODOTTO LA PERC DEVE ESSERE SUPERIORE AL 33%");
		hm.put("significativita_5", "L’INDICATORE MISURA IL MIGLIORAMENTO DEI TEMPI MEDI DEFINITO COME DIMINUIZIONE DEL TEMPO MEDIO DEI «PRODOTTI» REALIZZATI NELL’ANNO RISPETTO A QUELLI REALIZZATI NELL’ANNO PRECEDENTE");
		
		hm.put("ambito_6","TEMPI");
		hm.put("calcolo_6", "Tempo medio processo anno (n-1) – tempo medio processo  anno (n)\r\n" + 
				"\r\n" + 
				"DIVISO\r\n" + 
				"\r\n" + 
				"Tempo medio per processo anno (n-1) + Tempo medio per processo anno n");
		hm.put("target_6", "IL TARGET E’ ESPRESSO IN VALORI PERCENTUALI. PER AVERE LA META’ DEL TEMPO MEDIO PER PROCESSO LA PERC DEVE ESSERE SUPERIORE AL 33%");
		hm.put("significativita_6", "L’INDICATORE MISURA IL MIGLIORAMENTO DEI TEMPI DEFINITO COME DIMINUIZIONE DEL TEMPO MEDIO DEI «PROCESSI» REALIZZATI NELL’ANNO RISPETTO A QUELLI REALIZZATI NELL’ANNO PRECEDENTE");
		
		hm.put("ambito_7","QUALITA'");
		hm.put("calcolo_7", "	\r\n" + 
				"N. di prodotti (realizzati con tempi superiori ai tempi standard) anno (n) – N. di prodotti (realizzati con tempi superiori ai tempi standard) anno (n-1)\r\n" + 
				"\r\n" + 
				" DIVISO\r\n" + 
				"\r\n" + 
				"N. di prodotti (realizzati con tempi superiori ai tempi standard) anno (n) + N. di prodotti (realizzati con tempi superiori ai tempi standard) anno (n-1)");
		hm.put("target_7", "IL TARGET E’ ESPRESSO IN VALORI PERCENTUALI. PER AVERE LA RIDUZIONE DELLA LA META’ DEI PRODOTTI REALIZZATI CON TEMPI SUPERIORI A QUELLI STANDARD LA PERC DEVE ESSERE SUPERIORE AL 33%");
		hm.put("significativita_7", "L’INDICATORE MISURA IL MIGLIORAMENTO DELLA QUALITA’ DEFINITA COME DIMINUZIONE/AZZERAMENTO DEI PROOTTI REALIZZATI NELL’ANNO N CON TEMPI SUPERIORI A QUELLI DEFINITI DA REGOLAMENTI/NORME (ASSUNTI COME STANDARD DI QUALITA’) RISPETTO A QUELLI REALIZZATI NELL’ANNO PRECEDENTE");
		
		hm.put("ambito_8","QUALITA'");
		hm.put("calcolo_8", "N. di prodotti (realizzati con tempi superiori ai tempi standard) anno (n) – N. di prodotti (realizzati con tempi critici) anno (n-1)\r\n" + 
				"\r\n" + 
				" DIVISO\r\n" + 
				"\r\n" + 
				"N. di prodotti (realizzati con tempi superiori ai tempi standard) anno (n) + N. di prodotti (realizzati con tempi critici) anno (n-1)");
		hm.put("target_8", "IL TARGET E’ ESPRESSO IN VALORI PERCENTUALI. PER AVERE LA RIDUZIONE DELLA LA META’ DEI PRODOTTI REALIZZATI CON TEMPI CRITICI LA PERC DEVE ESSERE SUPERIORE AL 33%");
		hm.put("significativita_8", "L’INDICATORE MISURA IL MIGLIORAMENTO DELLA QUALITA’ COME DIMINUZIONE/AZZERAMENTO DEI PROOTTI REALIZZATI NELL’ANNO N CON TEMPI CRITICI (DEFINITI COME TALI DA VALUTAZIONI DI DIVERSA NATURA) RISPETTO A QUELLI REALIZZATI NELL’ANNO PRECEDENTE");
		
		return hm;		
	}
	
}
