package it.anac.obiettivi.backend.rest;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.exceptions.CsvValidationException;

import it.anac.obiettivi.backend.engine.Transcoding;
import it.anac.obiettivi.backend.model.Dipendenti;


@RestController
@RequestMapping(path="/ws")
public class Organizzazione extends RestConsts {
	
	@Autowired
	private Transcoding transcoding;
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@GetMapping("/dirigente-da-ufficio")
	public Dipendenti getDirigenteFromUfficio(String ufficio, String limit, String skip) throws IOException, CsvValidationException
	{
		return transcoding.getDirigenteFromUfficio(ufficio);		
	}
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@GetMapping("/dipendenti-da-ufficio")
	public Dipendenti getDipendentiFromUfficio(String ufficio, String limit, String skip) throws IOException, CsvValidationException
	{
		Dipendenti out = transcoding.getDipendentiFromUfficio(ufficio);
		return out;
	}
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@GetMapping("/impiegati-da-ufficio")
	public Dipendenti getImpiegatiFromUfficio(String ufficio, String limit, String skip) throws IOException, CsvValidationException
	{
		Dipendenti out = transcoding.getImpiegatiFromUfficio(ufficio);
		return out;
	}
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@GetMapping("/funzionari-da-ufficio")
	public Dipendenti getFunzionariFromUfficio(String ufficio, String limit, String skip) throws IOException, CsvValidationException
	{
		Dipendenti out = transcoding.getFunzionariFromUfficio(ufficio);
		return out;
	}
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@GetMapping("/ufficio-da-codice")
	public String getUfficioDaCodice(String codice, String limit, String skip) throws IOException, CsvValidationException
	{
		String out = transcoding.getUfficioFromCode(codice);
		return out;
	}
}
