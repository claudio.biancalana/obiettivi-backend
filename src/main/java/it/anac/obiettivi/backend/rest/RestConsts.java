package it.anac.obiettivi.backend.rest;

public class RestConsts {
	
	protected static final String CORS_HOST_PRD = "https://obiettivi2022.anticorruzione.it";
	protected static final String CORS_HOST_RIL = "http://obiettivi-ril.apps.ocp.premaster.local";
	protected static final String CORS_HOST_QLF = "https://obiettivi-qlf.anticorruzione.it";
	protected static final String CORS_HOST_PRE = "http://obiettivi-pre.apps.ocp.premaster.local";
	protected static final String CORS_HOST_DEV = "http://localhost:4200";
	
}
