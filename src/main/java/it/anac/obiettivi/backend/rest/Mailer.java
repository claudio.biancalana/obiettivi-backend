package it.anac.obiettivi.backend.rest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.StringTokenizer;

import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.exceptions.CsvValidationException;

import it.anac.obiettivi.backend.engine.MailSenderHelper;
import it.anac.obiettivi.backend.engine.Transcoding;


@RestController
@RequestMapping(path="/ws")
public class Mailer extends RestConsts {

	@Autowired
	private MailSenderHelper msh;
	
	@Value("${obiettivi.email}")
    private String email;
	
	@Value("${obiettivi.mode}")
    private String mode;
	
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@GetMapping("/mail")
	public void sendMail(String message) throws MessagingException
	{
		if (!email.contains(";"))
			msh.sendMessageBackground(email, "Invio obiettivo 2022", message, "", "");
		else
		{
			LinkedList<String> aux = new LinkedList<String>();
			StringTokenizer st = new StringTokenizer(email,";");
			while(st.hasMoreTokens())
				aux.add(st.nextToken());
			
			String[] to = new String[aux.size()];
			for(int i=0; i<aux.size(); i++)
				to[i]=aux.get(i);
			
			msh.sendMessageBackground(to, "Invio obiettivo 2022", message, "", "");
		}
	}
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@GetMapping("/mail_to")
	public void sendMail(String message, String emails) throws MessagingException
	{
		if (!emails.contains(";"))
			msh.sendMessageBackground(emails, "Invio obiettivo 2022", message, "", "");
		else
		{
			LinkedList<String> aux = new LinkedList<String>();
			StringTokenizer st = new StringTokenizer(emails,";");
			while(st.hasMoreTokens())
				aux.add(st.nextToken());
			
			String[] to = new String[aux.size()];
			for(int i=0; i<aux.size(); i++)
				to[i]=aux.get(i);
			
			msh.sendMessageBackground(to, "Scheda obiettivo 2022", message, "", "");
		}
	}
	
	@CrossOrigin(origins = {CORS_HOST_DEV,CORS_HOST_PRD,CORS_HOST_RIL,CORS_HOST_QLF,CORS_HOST_PRE})
	@GetMapping("/mail_to_dirigenti")
	public void sendMailDirigenti(String message, String dirigenti, String urlPath) throws MessagingException,
																						   IOException,
																						   CsvValidationException
	{
			LinkedList<String> aux = new LinkedList<String>();
			Transcoding t = new Transcoding();
			StringTokenizer st = new StringTokenizer(dirigenti,",");
			while(st.hasMoreTokens())
			{
				if (mode.toLowerCase().equals("test"))
					System.out.println("Invio a -> "+t.getMailFromDirigente(""+st.nextToken()));
				else
					aux.add(t.getMailFromDirigente(""+st.nextToken()));
			}

			
			String[] to = new String[aux.size()];
			for(int i=0; i<aux.size(); i++)
				to[i]=aux.get(i);
			
			File f = new File("pdf/report_"+System.currentTimeMillis()+".pdf");
			URL url = new URL(urlPath);
			FileUtils.copyURLToFile(url, f);
			
			msh.sendMessageBackground(to, "Scheda obiettivo 2022", message, f.getName(), f.getPath());
	}
}